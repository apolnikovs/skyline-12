DROP PROCEDURE IF EXISTS trigger_update;

DELIMITER ;;

CREATE PROCEDURE trigger_update(IN oldData TEXT, IN newData TEXT, IN tableName VARCHAR(100), IN fieldName VARCHAR(100), IN primaryID INT(11), IN userID INT(11))
BEGIN

	IF userID IS NULL
	THEN SET userID = 0;
	END IF;

	IF 		(newData != oldData OR (newData IS NULL AND oldData IS NOT NULL) OR (newData IS NOT NULL AND oldData IS NULL))
	THEN 	INSERT INTO audit_new 
						(
							UserID, 
							TableName,
							`Action`, 
							PrimaryID, 
							FieldName, 
							OldValue,
							NewValue
						) 
			VALUES 		
						(
							userID,
							tableName,
							'update',
							primaryID,
							fieldName,
							oldData,
							newData
						);
	END IF;

END
;;

DELIMITER ;