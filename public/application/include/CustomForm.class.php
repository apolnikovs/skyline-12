<?php

/**
 * Short Description of CustomForm.
 * 
 * Long description of CustomForm.
 *
 * @author     Brian Etherington <brian@smokingun.co.uk>
 * @copyright  2011 Smokingun Graphics
 * @link       http://www.smokingun.co.uk
 * @version    1.0
 *
 * Changes
 * Date        Version Author                Reason
 * 23/01/2012  1.0     Brian Etherington     Initial Version
 ******************************************************************************/

abstract class CustomForm {
    
    protected $controller;

    private $_data = array();
    private $_reset = array();
    //private $_sanitizeChain = null;

    public $Error = '';

    public function  __construct($controller) {
        
        $this->controller = $controller;
        $this->Initialise();

        // Load Form Items with post Values...
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            foreach ($this->_data as &$item) {

                if (array_key_exists($item['name'], $_POST)) {

                    // Step 1. update item value
                    $item['value'] = $this->sanitize($_POST[$item['name']]);

                    // Step 2. update choice item selection
                    if (is_array($item['options'])) {
                        $item['selected'] = $item['value'];
                    } else {
                        $item['selected'] = true;
                    }
                } else {

                    // if checkbox set it unchecked
                    if (!is_array($item['options'])) {
                        $item['selected'] = false;
                    }
                }
            }
        }

        // Process Form Item values
        foreach ($this->_data as &$item) {

            // call process routines
            $method = 'process_' . $item['name'];
            if (method_exists($this, $method)) {
                $this->$method($item['value']);
            }
        }

    }

    abstract function Initialise();

    public function sanitize($value) {

        /*if (!$this->_sanitizeChain instanceof Zend_Filter) {
            $this->_sanitizeChain = new Zend_Filter();
            $this->_sanitizeChain->addFilter(new Zend_Filter_StringTrim())
                    ->addFilter(new Zend_Filter_StripTags());
        }

        // filter out any line feeds / carriage returns
        //$ret = preg_replace('/[\r\n]+/', ' ', $value);

        // filter using the above chain
        return $this->_sanitizeChain->filter($value);*/
        return $value;
    }

    public function get_data() {
        return $this->_data;
    }

    public function IsValid() {

        $result = true;

        foreach ($this->_data as &$item) {

            // Step 4a. check for required fields
            if ($item['required'] &&
                    ((!is_array($item['options']) && $item['value'] == '') ||
                    (is_array($item['options']) && $item['selected'] == ''))) {
                $item['error'] = 'This field is required.';
                $result = false;
            } else {
                // Step 4b.call item validation routines
                $method = 'validate_' . $item['name'];
                if (method_exists($this, $method)) {
                    $item['error'] = $this->$method($item['value']);
                    if ($item['error'] != '') {
                        $result = false;
                    }
                }
            }
        }

        return $result;
    }

    public function __get($property) {

        if (array_key_exists($property, $this->_data)) {
            return $this->_data[$property]['value'];
        }

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $property .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);

        return null;
    }

    public function __set($property, $value) {
        $this->_data[$property]['value'] = $value;
    }


    public function SetError($name, $message) {
        $this->Set($name, 'error', $message);
    }

    public function SetOptions($name, $options, $value='') {
        $this->Set($name, 'options', $options);
        $this->SetValue($name, $value);
    }

    public function SetSelected($name) {
        if (!is_array($this->_data[$name]['options'])) {
            $this->Set($name, 'selected', true);
        } else {
            $this->_data[$name]['selected'] = $this->_data[$name]['value'];
        }
    }

    public function SetValue($name, $value) {
        $this->Set($name, 'value', $value);
        $this->SetSelected($name);
    }

    public function Reset($name) {
        $this->_data[$name] = $this->_reset[$name];
    }

    public function AddItem($name, $required, $value, $maxlen) {
        $this->Add($name, $required, $value, null, '', $maxlen);
    }

    public function AddChoice($name, $required, $options, $selected) {
        $this->Add($name, $required, '', $options, $selected, 0);
    }

    private function Add($name, $required, $value, $options, $selected, $maxlen) {

        $item = array('name' => $name,
                      'required' => $required,
                      'error' => '',
                      'value' => $value,
                      'options' => $options,
                      'selected' => $selected,
                      'maxlen' => $maxlen);

        $this->_data[$name] = $item;
        $this->_reset[$name] = $item;
    }

    private function Set($name, $key, $value) {
        $this->_data[$name][$key] = $value;
        $this->_reset[$name][$key] = $value;
    }

}
?>
