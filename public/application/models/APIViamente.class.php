<?php

/**
 * APIViamente.class.php 
 * 
 * Database access routines for building queries for the Viamente rotuing system
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @author     Andris Polnikovs <a.polnikovs@pccsuk.com>
 * @copyright  2012 - 2013 PC Control Systems
 * @link 
 * @version    1.39
 * 
 * Changes
 * Date        Version Author                Reason
 * 28/09/2012  1.00    Andrew J. Williams    Initial Version
 * 17/10/2012  1.01    Andrew J. Williams    Issue 100 - Viamente API is returning incorrect Skill set data.
 * 19/10/2012  1.02    Andrew J. Williams    Issue 103 - Route Colour for Engineers not being applied on Viamente
 * 22/10/2012  1.03    Andrew J. Williams    Issue 104 - Fields for Enginer Start / End postcode in Viamente API
 * 29/10/2013  1.03A   Andris Polnikovs      Added cleanOldValues
 * 30/10/2012  1.04    Andrew J. Williams    Read API key from record in service_provider table
 * 02/11/2012  1.05    Andrew J. Williams    Issue 116 - Viamente API not sending collection address
 * 12/11/2012  1.06    Andrew J. Williams    BRS 114 - Use Travel Speed setting in service_providers table
 * 12/11/2012  1.07    Andrew J. Williams    BRS 114 - Viamente API will create skillset tag with engineer name when engineer created / updated to allow
 * 12/11/2012  1.08    Andrew J. Williams    BRS 114 - Pass priority of appointment to Viamente in optimisation phase
 * 15/11/2012  1.09    Andrew J. Williams    BRS 114 - Allow engineer to be specified when optimising
 * 15/11/2012  1.10    Andrew J. Williams    BRS 114 - Allow optimisation of selected engineers only
 * 16/11/2012  1.11    Andrew J. Williams    Issue 132 - Viamente API - Process Engineer Failing
 * 18/11/2012  1.12    Andris Polnikovs      specify engineer
 * 19/11/2012  1.13    Andrew J. Williams    Replaceed and fixed bug in selection by engineer which Andris had commented out
 * 19/11/2012  1.14    Andrew J. Williams    BRS 114 - Allow indivdual start and finish times by engineer.
 * 21/11/2012  1.15    Andris Polnikovs      fixed specify engineer
 * 27/11/2012  1.16    Andrew J. Williams    Issue 142 - Individual engineer routing not working in Diary
 * 28/11/2012  1.17    Andrew J. Williams    Individual engineer optimise will take into account unassigned appointments
 * 29/11/2012  1.18    Andrew J. Williams    Added Andris's additional fields (timeDepart, timeArrive) to optimise individual engineer
 * 05/12/2012  1.19    Andrew J. Williams    Issue 148 - Viamente Optimise Routes to allow two Time Windows
 * 11/12/2012  1.20    Andrew J. Williams    Bug in Process Engineers Query due to Nag's database change
 * 17/12/2012  1.21    Andrew J. Williams    Trackerbase VMS 44 - Deduct the service interval from the time window 
 * 10/01/2012  1.22    Andrew J. Williams    Engineer's holidays taken into account in appointment route optimisation
 * 24/01/2013  1.23    Andrew J. Williams    Issue 182 - API Viamente - Latutude -> Latitude
 * 28/01/2013  1.24    Andrew J. Williams    Issue 187 - Viamente Error When Updating Engineer in Defaults
 * 28/01/2013  1.25    Andrew J. Williams    Issue 188 - Engineer cost field
 * 29/01/2013  1.26    Andrew J. Williams    Issue 190 - Speed Factor not Calculating Properly in Route Optimisation
 * 30/01/2013  1.27    Andris Polnikovs      Added exclude engineer
 * 05/02/2013  1.28    Andrew J. Williams    Issue 198 - Deal with overlapping break times for engineer's holiday
 * 29/01/2013  1.29    Andrew J. Williams    Issue 190 - Speed Factor not Calculating Properly in Route Optimisation (code lost and reinstated)
 * 25/02/2013  1.30    Andrew J. Williams    Rob's changes to diary lunch / holiday break overlap
 * 28/02/2013  1.31    Andrew J. Williams    Issue 238 - One Touch Interface - Run Viamente after appointment insert
 * 07/03/2013  1.32    Andris Polnikovs      Exclude completed appointments from viamente optimisation
 * 08/03/2013  1.33    Andris Polnikovs      Process engineer if today using gps location and current time
 * 10/03/2013  1.34    Andris Polnikovs      fixed/flexible time default 
 * 21/03/2012  1.35    Andrew J. Williams    Fixed Procesess engineer always reporting failure
 * 03/04/2013  1.36    Andrew J. Williams    Delete Viamente vehicle (engineer) by name for Rob Perry's testing
 * 26/04/2013  1.37    Andrew J. Williams    Issue 324 - Change Viamente Process to use Matrix Optimisation Instead of One Shot
 * 31/05/2013  1.38    Andris Polnikovs      Sending postcodes as tags with appointment and process engineer
 * 03/06/2013  1.39    Andris Polnikovs      Not adding anymore postcodes and skillsets to appointment required list if engineer is specified by user
 * 10/06/2013  1.40    Dileep Bhimineni      Check to see if slider start/end time is available otherwise use Default start/end time
 * **************************************************************************** */
require_once('CustomModel.class.php');
include_once(APPLICATION_PATH . '/models/PostcodeLatLong.class.php');

class APIViamente extends CustomModel {

    private $pc;
    private $api_key;

    public function __construct($controller) {

        parent::__construct($controller);

        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);

        $this->debug = true;

        $this->table_appointment = TableFactory::Appointment();
        $this->table_diary_allocation = TableFactory::DiaryAllocation();

        $this->pc = new PostcodeLatLong($controller);

        if (isset($this->controller->user->ServiceProviderID)) {
            $this->api_key = $this->setViamenteKey($this->controller->user->ServiceProviderID);
        } elseif (isset($this->controller->ServiceProviderID)) {
            $this->api_key = $this->setViamenteKey($this->controller->ServiceProviderID);
        }
        if ($this->debug)
            $this->controller->log($this->api_key, "key");
    }

    /**
     * setViamenteKey
     *  
     * Set the viamente key for the given service provider. this must me set before
     * anyh api callas are made.
     * 
     * @param inter $spId   The ID of the service provider we want the key for
     * 
     * @return Nothing
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    private function setViamenteKey($spId) {
        $sql = "
                SELECT
			`ViamenteKey`,
                         ViamenteKey2
		FROM
			`service_provider`
		WHERE
			`ServiceProviderID` = $spId
               ";

        if ($this->debug)
            $this->controller->log("APIViamente::setViamenteKey - Query \n$sql", 'viamente_api_');

        $result = $this->Query($this->conn, $sql);

        if ($this->debug)
            $this->controller->log("APIViamente::setViamenteKey - Query Result \n" . var_export($result, true), 'viamente_api_');

        if (count($result) > 0) {
            $this->api_key2 = $result[0]['ViamenteKey2'];
            return($result[0]['ViamenteKey']);                                  /* Viamete key exists */
        } else {
            $this->controller->log("APIViamente::setViamenteKey - Can't find Viamente key for $spId ", 'viamente_api_');
            return("");
        }
    }

    /**
     * ProcessEngineer
     *  
     * Create a new or update an existing engineer (vehicle) in the Viamente system
     * 
     * @param $eId      Service Provider Engineer ID 
     *        $medthod  Requiest type (create or update)
     *        $today default false, if true update only for today using sp_engineer_day_location to set new start location
     * 
     * @return Array containing output of the API call
     *         Will return name if sucessful
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function ProcessEngineer($eId, $method, $today = false) {
        if ($this->debug)
            $this->controller->log("APIViamente::ProcessEngineer - START --------------------", 'viamente_api_');

        /* Begin Change - 30/12/2013 - Andris Polnikovs - Exclude Engineer */
        $sql = "select * from service_provider_engineer spe where ServiceProviderEngineerID=$eId";
        $excludet = $this->Query($this->conn, $sql);
        $output = "";
        if (isset($excludet[0]) && $excludet[0]['ViamenteExclude'] == 'No') {
            /* End Change - 30/12/2013 - Andris Polnikovs - Exclude Engineer */

            $now = date('Y-m-d');                                                   /* Todays date */
            $output = "";

            $dates = array();                                                       /* Array to hold dates for the next week */
            if (!$today) {
                $daysNames = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
            } else {
                $daysNames = array($dayOfWeek = date('l', strtotime("today")));
            }

            for ($dayCount = 0; $dayCount < count($daysNames); $dayCount++) {
                $dates[date('l', strtotime("+$dayCount day"))] = date('Y-m-d', strtotime("+$dayCount day"));
            }



            $sql = "
                    SELECT 
                            CONCAT(spe.`EngineerFirstName`,' ',`EngineerLastName`) AS `name`,
                            TIME_TO_SEC(spe.`StartShift`) AS `startTimeSecs`, 
                            TIME_TO_SEC(spe.`EndShift`) AS `stopTimeSecs`,
                            IF(spe.`StartLocation` = 'Home',
                                    spe.`StartHomePostcode`
                            ,
                                    IF(spe.`StartLocation` = 'Office',
                                            spe.`StartOfficePostcode`
                                    ,
                                            spe.`StartOtherPostcode`
                                    )
                            ) AS `origin`,
                            spe.PrimarySkill,
                            spe.ServiceProviderID,
                            IF(spe.`EndLocation` = 'Home',
                                    spe.`EndHomePostcode`
                            ,
                                    IF(spe.`EndLocation` = 'Office',
                                            spe.`EndOfficePostcode`
                                    ,
                                            spe.`EndOtherPostcode`
                                    )
                            ) AS `destination`,
                            spe.`SpeedFactor`,
                            spe.`EngineerImportance`,
                            spe.`ViamenteStartType`,
                            ";
            

            foreach ($daysNames AS $day) {
                
                 /*
                * @author dileep Bhimineni.
                * 
                * Check if slider start/end time is available use it otherwise use Default start/end time
                *  
               */

                if (!$today) {

                    $sql.= '
                       sped_' . $day . '.`Status` AS `' . $day . 'Status`,
                       CASE WHEN sped_' . $day . '.`SliderStartShift` IS NULL or 
                            sped_' . $day . '.SliderStartShift = "" 
                            THEN TIME_TO_SEC(sped_' .  $day . '.`StartShift`)
                            ELSE  TIME_TO_SEC(sped_' . $day . '.`SliderStartShift`) END
                       AS `' . $day . 'StartTimeSecs`,
                       CASE WHEN sped_' . $day . '.`SliderEndShift` IS NULL or 
                            sped_' . $day . '.SliderEndShift = "" 
                            THEN TIME_TO_SEC(sped_' .  $day . '.`EndShift`)
                            ELSE  TIME_TO_SEC(sped_' . $day . '.`SliderEndShift`) END
                       AS `' . $day . 'StopTimeSecs`,
                       sped_' . $day . '.`StartPostcode` AS `' . $day . 'Origin`, 
                       sped_' . $day . '.`EndPostcode` AS `' . $day . 'Destination`,';
                } else {
                    $sql.= '
                       sped_' . $day . '.`Status` AS `' . $day . 'Status`,
                       CASE WHEN sped_' . $day . '.`SliderStartShift` IS NULL or 
                            sped_' . $day . '.SliderStartShift = "" 
                            THEN TIME_TO_SEC(CURTIME())
                            ELSE  TIME_TO_SEC(sped_' . $day . '.`SliderStartShift`) END
                       AS `' . $day . 'StartTimeSecs`,
                       CASE WHEN sped_' . $day . '.`SliderEndShift` IS NULL or 
                            sped_' . $day . '.SliderEndShift = "" 
                            THEN TIME_TO_SEC(sped_' .  $day . '.`EndShift`)
                            ELSE  TIME_TO_SEC(sped_' . $day . '.`SliderEndShift`) END
                       AS `' . $day . 'StopTimeSecs`,
                       
                       if( concat_ws(",",spedl.Langitude,spedl.Longitude)="",sped_' . $day . '.`StartPostcode`,concat_ws(",",spedl.Langitude,spedl.Longitude))
                         AS `' . $day . 'Origin`, 
                       sped_' . $day . '.`EndPostcode` AS `' . $day . 'Destination`,
                        LunchTaken,   
                        ';
                }
            }


            $sql .= "
                            TIME_TO_SEC(spe.`LunchPeriodFrom`) AS `startTimeSecLunch`,
                            TIME_TO_SEC(spe.`LunchPeriodTo`) AS `endTimeSecLunch`,
                            spe.`LunchBreakDuration` * 60 AS `durationSecLunch`,
                            spe.`RouteColour` AS `routeColorHex`
                           
                    FROM
                            `service_provider_engineer` spe ";



            foreach ($dates as $day => $date) {
                $sql .= "LEFT JOIN `service_provider_engineer_details` sped_$day ON spe.`ServiceProviderEngineerID` = sped_$day.`ServiceProviderEngineerID` 
                                                                                    AND sped_$day.`WorkDate` = '$date' ";
                if ($today) {
                    $sql .= "LEFT JOIN `sp_engineer_day_location` spedl ON spe.`ServiceProviderEngineerID` = spedl.`ServiceProviderEngineerID`
                    and     DATE_FORMAT(spedl.`DateModified`,'%d')=DATE_FORMAT(curdate(),'%d')
                            ";
                }
            }
            $sql.= "
                    WHERE
                            spe.`ServiceProviderEngineerID` = $eId  
                   ";
            if ($today) {
                $sql.="";
            }

            if ($this->debug)
                $this->controller->log("APIViamente::ProcessEngineer - Query SQLt \n$sql", 'viamente_procesEngineer_');

            $result = $this->Query($this->conn, $sql);
            $engdata = $result[0];
//            echo"<pre>";
//            print_r($result);
//            echo"</pre>";
            if (count($result) == 0) {
                if ($this->debug)
                    $this->controller->log("APIViamente::ProcessEngineer - No Such Engineer $eId", 'viamente_api_');
                return("APIViamente::ProcessEngineer - No Such Engineer $eId");
            }

            $result = $result[0];

            if ($this->debug)
                $this->controller->log("APIViamente::ProcessEngineer - Query Result \n" . var_export($result, true), 'viamente_api_');
            foreach ($dates as $day => $date) { /* Loop through each date required */
                if ($result[$day . 'Status'] == 'Active') { /* Check if the engineer (vehicle) is active for this day */
                    if ($result['ViamenteStartType'] == 'Fixed' || $today) {
                        $fixed = 'true';
                    } else {
                        $fixed = 'false';
                    } //check start time type
                    $params = array(/* Yes it is so process */
                        'name' => $result['name'] . '_' . $day,
                        'routeColorHex' => $result['routeColorHex'],
                        'timeWindow' => array(/* Shift */
                            'startTimeSec' => intval($result[$day . 'StartTimeSecs']),
                            'stopTimeSec' => intval($result[$day . 'StopTimeSecs'])
                        ),
                        'timeCostFactor' => intval($result['EngineerImportance']),
                        'fixedStartTime' => $fixed
                    );


                    $engName = $result['name'];
                    if (!$today) {
                        if ($result['durationSecLunch'] != 0) { /* Deal with lunch break if set */
                            $lunchStartTimeSec = intval($result['startTimeSecLunch']);
                            $lunchEndTimeSec = intval($result['endTimeSecLunch']) - intval($result['durationSecLunch']);

                            if ($lunchStartTimeSec == 0)
                                $lunchStartTimeSec = intval($result[$day . 'StartTimeSecs']) + 60;
                            if ($lunchEndTimeSec == 0)
                                $lunchEndTimeSec = intval($result[$day . 'StopTimeSecs']) - 60 - $result['durationSecLunch'];
                            $lunchDuration = intval($result['durationSecLunch']);

                            /* Lunch duration is now calculated but, we will only add to API parametrs after we have check holiday breaks as the Viamente
                             * API does not allow overlapping breaks. See Issue 198 at https://bitbucket.org/pccs/skyline/issue/198/deal-with-overlapping-break-times-for  */
                        }
                    }else {
                        if ($result['durationSecLunch'] != 0 && $result['LunchTaken'] != "Yes") { /* Deal with lunch break if set and day is today
                         * giving engineer break as soon as he finish appointment in break window */

                            $lunchStartTimeSec = intval($result['startTimeSecLunch']);
                            $lunchEndTimeSec = intval($result['endTimeSecLunch']) - intval($result['durationSecLunch']);

                            if ($lunchStartTimeSec == 0)
                                $lunchStartTimeSec = intval($result[$day . 'StartTimeSecs']) + 60;
                            if ($lunchEndTimeSec == 0)
                                $lunchEndTimeSec = intval($result[$day . 'StopTimeSecs']) - 60 - $result['durationSecLunch'];
                            $lunchDuration = intval($result['durationSecLunch']);


                            if ($lunchStartTimeSec < $result[$day . 'StartTimeSecs'] * 1) {

                                $lunchStartTimeSec = intval($result[$day . 'StartTimeSecs']) + 1;
                                $lunchEndTimeSec = intval($result[$day . 'StartTimeSecs']) + intval($result['durationSecLunch']) + 2;
                                $query = "update sp_engineer_day_location set LunchTaken='Yes' where ServiceProviderEngineerID=$eId";
                                $this->Execute($this->conn, $query);
                            }
                        }
                    }
                    if (preg_match("^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$^", $result[$day . 'Origin'])) {
                        $geotag = explode(',', $result[$day . 'Origin']);
                        $params['origin'] = array('latitude' => $geotag[0], 'longitude' => $geotag[1]);
                    } else {
                        $params['origin'] = $this->pc->getLatLong($result[$day . 'Origin']);
                    }
                    if (preg_match("^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$^", $result[$day . 'Destination'])) {
                        $geotag = explode(',', $result[$day . 'Destination']);
                        $params['destination'] = array('latitude' => $geotag[0], 'longitude' => $geotag[1]);
                    } else {
                        $params['destination'] = $this->pc->getLatLong($result[$day . 'Destination']);
                    }

                    $params['speedFactor'] = floatval($result['SpeedFactor'] / 100);

                    if ($this->debug)
                        $this->controller->log("APIViamente::ProcessEngineer - Query Result \n" . var_export($params, true), 'viamente_procesEngineer_');


                    /* Get holiday specifc date breaks for current engneer and date */
                    $sqlBreaks = "                                                  
                                  SELECT
                                            `StartTimeSec`,
                                            `EndTimeSec`,
                                            `TotalTimeSec`
                                  FROM
                                            `diary_holiday_slots` dhs
                                  WHERE
                                            `ServiceProviderEngineerID` = $eId
                                            AND `HolidayDate` = '$date'
                                 ";

                    if ($this->debug)
                        $this->controller->log("APIViamente::ProcessEngineer - Holidays Query \n" . $sqlBreaks, 'viamente_api_');

                    $resultBreaks = $this->Query($this->conn, $sqlBreaks);

                    if ($this->debug)
                        $this->controller->log("APIViamente::ProcessEngineer - Holiday Query Result \n" . var_export($resultBreaks, true), 'viamente_api_');
                    if (!isset($lunchDuration)) {
                        $lunchDuration = 0;
                        $lunchStartTimeSec = 0;
                        $lunchEndTimeSec = 0;
                    }
                    for ($b = 0; $b < count($resultBreaks); $b++) {

                        $breakStartTimeSec = intval($resultBreaks[$b]['StartTimeSec']);
                        $breakLength = intval($resultBreaks[$b]['TotalTimeSec']);

                        if ($breakStartTimeSec < $result[$day . 'StartTimeSecs'])
                            $breakStartTimeSec = intval($result[$day . 'StartTimeSecs']) + 1;
                        if ($breakStartTimeSec + $breakLength > $result[$day . 'StopTimeSecs'])
                            $breakLength = $result[$day . 'StopTimeSecs'] - $breakStartTimeSec;

                        if (!isset($params['breaks']))
                            $params['breaks'] = array();

                        /* If there is a holiday break ignore lunch if overlaps */
                        if (
                                ( /* Lunch Inside holiday */
                                ($lunchStartTimeSec >= $breakStartTimeSec)
                                && ($lunchStartTimeSec + $lunchDuration <= $breakStartTimeSec + $breakLength)
                                )
                                ||
                                ( /* Lunch covers holiday */
                                ($lunchStartTimeSec <= $breakStartTimeSec)
                                && ($lunchEndTimeSec + $lunchDuration >= $breakStartTimeSec + $breakLength)
                                )
                                ||
                                (
                                ($lunchStartTimeSec >= $breakStartTimeSec) /* Lunch starts in holiday */
                                && ($lunchStartTimeSec <= $breakStartTimeSec + $breakLength)
                                )
                                ||
                                (
                                ($lunchEndTimeSec + $lunchDuration >= $breakStartTimeSec) /* Lunch ends in holiday */
                                && ($lunchEndTimeSec + $lunchDuration <= $breakStartTimeSec + $breakLength)
                                )
                        ) {
                            $lunchDuration = 0;
                        } /* fi there is a holiday break ignore lunch */

                        $params['breaks'][] = array(
                            'startTimeSec' => $breakStartTimeSec,
                            'stopTimeSec' => $breakStartTimeSec,
                            'durationSec' => $breakLength
                        );
                    }

                    if (isset($lunchDuration) && ($lunchDuration > 0)) {
                        if (!isset($params['breaks']))
                            $params['breaks'] = array();

                        $params['breaks'][] = array(/* Write calculated lunch break times */
                            'startTimeSec' => $lunchStartTimeSec,
                            'stopTimeSec' => $lunchEndTimeSec,
                            'durationSec' => $lunchDuration
                        );
                    }

                    /* Get tags (skillsets) */

                    $sqlSkillSet = "
                                SELECT 
                                    ss.`SkillsetName` 
                                FROM 
                                    `skillset` ss, 
                                    `service_provider_engineer` spe,
                                    `service_provider_engineer_details` sped,
                                    `service_provider_skillset` sps,  
                                    `service_provider_engineer_skillset` spes, 
                                    `service_provider_engineer_skillset_day` spesd
                                WHERE 
                                    spe.`ServiceProviderEngineerID` = sped.`ServiceProviderEngineerID`
                                    AND sped.`ServiceProviderEngineerDetailsID` =  spesd.`ServiceProviderEngineerDetailsID`
                                    AND spesd.`ServiceProviderSkillsetID` = sps.`ServiceProviderSkillsetID`
                                    AND sps.`SkillsetID` = ss.`SkillsetID`
                                    AND spe.`ServiceProviderEngineerID` = $eId
                                    AND sped.`WorkDate` = '$date'
                            ";

                    $resultSkillSet = $this->Query($this->conn, $sqlSkillSet);

                    $params['tags'] = $resultSkillSet;                                      /* Tags are the names of the skillsets assigned to the engineer */
                    for ($f = 0; $f < count($resultSkillSet); $f++) {
                        $params['tags'][$f] = $resultSkillSet[$f]['SkillsetName'];
                    }
                    $params['tags'][$f] = $engName;                                         /* Additional hidden tag (skillset) is engineer name to allow engineer to fixed */


                    /* Get tags (postcodes) */

                    $diary_model = $this->controller->LoadModel('Diary');
                    $resultPostcodes = $diary_model->getEngineersPostcodes($eId, $date);
                    /* Tags are the names of the skillsets assigned to the engineer */
                    for ($ff = 0; $ff < count($resultPostcodes); $ff++) {

                        //$this->controller->log($params['tags'],"eng_postcodes");
                        $params['tags'][$ff + $f + 1] = $resultPostcodes[$ff]['pc'];
                    }


                    /*
                     * Create and Call REST Client
                     */

                    $params_json = json_encode($params, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
                    if ($this->debug)
                        $this->controller->log("APIViamente::ProcessEngineer - params = \n" . $params_json, 'viamente_apiProces_');
                    $key = $this->api_key;
                    if ($engdata['ServiceProviderID'] == 64) {
                        if ($engdata['PrimarySkill'] == "White Goods") {
                            $key = $this->api_key2;
                            $this->DeleteEngineer($eId);
                        } else {
                            $this->DeleteEngineer($eId, "White Goods");
                        }
                    }

                    if ($this->debug)
                        $this->controller->log($key, 'viamente_api_key_');
                    $engineerlist = $this->ListEngineers($key);                                 /* Get a current list of engineers */
                    if ($this->debug)
                        $this->controller->log("APIViamente::ProcessEngineer - Current Engineer lIST = " . var_export($engineerlist, true), 'viamente_api_');
                    switch (strtolower($method)) {
                        case 'create':
                            $output[$day] = $this->viamenteCall($params_json, '/opt/v3/vehicle', $key);
                            break;

                        case 'update':
                            if (!isset($engineerlist['vehicles'])) {
                                $engineerlist['vehicles'] = array();
                            }
                            if (in_array($engName . '_' . $day, $engineerlist['vehicles'])) { /* Updating - day for engineer might not exist - so check */
                                $output[$day] = $this->viamenteCall($params_json, "/opt/v3/vehicle/name/" . urlencode($engName . '_' . $day), $key, 'PUT');                 /* Call the update engineer API */
                            } else {
                                $output[$day] = $this->viamenteCall($params_json, '/opt/v3/vehicle', $key);
                            }

                            break;

                        default:
                            $output[$day]['Message'] = 'Unknown method';
                    }

                    if ($this->debug)
                        $this->controller->log('APIViamente::ProcessEngineer - REST Call response \n' . var_export($output[$day], true), 'viamente_api_');

                    /*
                     * Check and process REST output
                     */

                    if ($output[$day] === false) {
                        // Server failure
                        if ($this->debug)
                            $this->controller->log('REST Server Error: ' . var_export($output[$day], true));

                        return(
                                array(
                                    'Message' => 'REST Server Error'
                                )
                                );
                    } elseif (isset($output[$day])) { /* Successful response */
                        if ($this->debug)
                            $this->controller->log('APIViamente::ProcessEngineer - Success \n' . var_export($output[$day], true), 'viamente_api_');
                    } else { /* Failure response */
                        // $this->controller->log('APIViamente::ProcessEngineer - Failure');
                        if ($this->debug)
                            $this->controller->log('APIViamente::ProcessEngineer - Failure', 'viamente_api_');
                    }
                } else {
                    if (isset($engineerlist['vehicles'])) {
                        if (in_array($result['name'] . '_' . $day, $engineerlist['vehicles'])) {
                            $output = $this->viamenteCall('', "/opt/v3/vehicle/name/" . urlencode($result['name'] . '_' . $day), $key, 'DELETE');      /* Not active delete */
                        }
                    }
                } /* fi dayStaus == active */
            } /* next $day */

            /* Begin Change - 30/12/2013 - Andris Polnikovs - Exclude Engineer */
        } else {
            if ($this->debug)
                $this->controller->log("APIViamente::ProcessEngineer - Enigneer Excluded ", 'viamente_api_');
        }
        /* End Change - 30/12/2013 - Andris Polnikovs - Exclude Engineer */
        if ($this->debug)
            $this->controller->log("APIViamente::ProcessEngineer - END --------------------", 'viamente_api_');
        return($output);
    }

    /**
     * DeleteEngineer
     *  
     * Delete an engineer from viamente
     * 
     * @param $eId      Service Provider Engineer ID to delete
     * 
     * @return Array containing output of the API call
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function DeleteEngineer($eId, $type = false) {
        if (!$type) {
            $key = $this->api_key;
        } elseif ($type == "White Goods") {
            $key = $this->api_key2;
        }
        if ($this->debug)
            $this->controller->log("APIViamente::DeleteEngineer - START --------------------", 'viamente_api_');

        $daysNames = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');

        $sql = "
                SELECT 
			CONCAT(spe.`EngineerFirstName`,' ',`EngineerLastName`) AS `name`
                FROM
                        `service_provider_engineer` spe
                WHERE
			spe.`ServiceProviderEngineerID` = $eId
               ";

        if ($this->debug)
            $this->controller->log("APIViamente::DeleteEngineer - sql \n$sql", 'viamente_api_');

        $result = $this->Query($this->conn, $sql);

        if ($this->debug)
            $this->controller->log("APIViamente::DeleteEngineer - Query Result \n" . var_export($result, true), 'viamente_api_');

        if (count($result) > 0) {
            foreach ($daysNames AS $day) { /* Loop through each day */
                $output = $this->viamenteCall('', "/opt/v3/vehicle/name/" . urlencode($result[0]['name'] . '_' . $day), $key, 'DELETE');
            }
        } else {
            $output = "No such Engineer ID: $eId";
            if ($this->debug)
                $this->controller->log("APIViamente::DeleteEngineer - No such Engineer ID: $eId", 'viamente_api_');
        }

        if ($this->debug)
            $this->controller->log("APIViamente::DeleteEngineer - END --------------------", 'viamente_api_');
        return($output);
    }

    /**
     * DeleteEngineerByName
     *  
     * Delete an engineer from viamente by the engineer name. This is for Rob Perry
     * when testing allowing him to easily clear out an engineer from the API test 
     * key.
     * 
     * @param string $eName     The engineer name
     * @param string $key       The API key
     * 
     * @return Array containing output of the API call
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function DeleteEngineerByName($eName, $key) {
        if ($this->debug)
            $this->controller->log("APIViamente::DeleteEngineerByName - START --------------------", 'viamente_api_');

        $output = $this->viamenteCall('', "/opt/v3/vehicle/name/" . urlencode($eName), $key, 'DELETE');

        if ($this->debug)
            $this->controller->log("APIViamente::DeleteEngineerByName - API output" . var_export($output, true), 'viamente_api_');

        if ($this->debug)
            $this->controller->log("APIViamente::DeleteEngineerByName - END --------------------", 'viamente_api_');
        return($output);
    }

    /**
     * DeleteAllEngineersFromKey
     *  
     * Delete an engineer from viamente for key assigned to sp. This is for Rob Perry
     * when testing allowing him to easily clear out an engineer from the API test 
     * key.
     * 
     * @param string $eName     The engineer name
     * @param string $key       The API key
     * 
     * @return Array containing output of the API call
     * 
     * @author Andris Polnikovs <a.polnikovs@pccsuk.com>  
     * ************************************************************************ */
    public function DeleteAllKeyEngineers() {
        $key = $this->api_key;

        $englist = $this->ListEngineers();

        foreach ($englist['vehicles'] as $r) {

            $ret = $this->DeleteEngineerByName($r, $key);
            echo"<pre>";
            print_r($ret);
            echo"</pre>";
        }
        //$output = $this->viamenteCall('', "/opt/v3/vehicle/name/$eName", $key,'DELETE');
        //return($output);
    }

    /**
     * ListEngineers
     *  
     * List engineers in viamente
     * 
     * @param none
     * 
     * @return Array containing list of engineers (vehicles)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function ListEngineers($key = false) {
        if (!$key) {
            $key = $this->api_key;
        }
        if ($this->debug)
            $this->controller->log("APIViamente::ListEngineers - START --------------------", 'viamente_api_');
        if ($this->debug)
            $this->controller->log($key, 'viamente_api_');

        $output = $this->viamenteCall('', "/opt/v3/vehicle", $key, 'GET');

        if ($this->debug)
            $this->controller->log("APIViamente::ListEngineers - Output of List Engineers \n" . var_export($output, true), 'viamente_api_');

        if ($output === false) {
            // Server failure
            if ($this->debug)
                $this->controller->log('REST Server Error: ' . var_export($output, true), 'viamente_api_');

            return(
                    array(
                        'vehicles' => array(),
                        'Message' => 'REST Server Error'
                    )
                    );
        } else {
            $data = json_decode($output['response'], true);
            if (isset($data['vehicles'])) {
                $output['vehicles'] = array();

                $vehicles = $data['vehicles'];

                foreach ($vehicles as $i => $vehicle) {
                    $output['vehicles'][$i] = $vehicle['name'];
                }
            }
        }

        if ($this->debug)
            $this->controller->log("APIViamente::ListEngineers - END --------------------", 'viamente_api_');
        return($output);
    }

    /**
     * OptimiseRoute
     *  
     * Produce an optimised route
     * 
     * @param $spId     The ID of the service provider
     *        $date     The date rqequired
     *        $eIds     Array of engineers required
     * 
     * @return Array containing output of the API call
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function OptimiseRoute($spId, $date, $eIds, $skillType = false, $notUnlockTmpEngs = false) {
        $diary_matrix_model = $this->controller->LoadModel('DiaryMatrix');

        if ($this->debug)
            $this->controller->log("APIViamente::OptimiseRoute - START --------------------", 'viamente_api_');

        $postcodenotfound = array();

        $sql = "select sp.KeepEngInPCArea from service_provider sp where sp.ServiceProviderID=$spId"; //checking if sp is set to lock engineers postcodes areas
        $lock = $this->Query($this->conn, $sql);


        $dayOfWeek = date('l', strtotime($date));                              /* Get the day of the week */
        if (!isset($eIds[0])) {
            $this->controller->log("APIViamente::OptimiseRoute - NO ENGINEERS \n", 'viamente_api_error');
            return;
        }
        $engineer_where_clause = 'spe.`ServiceProviderEngineerID` = ' . $eIds[0];
        if (count($eIds) > 1) {
            for ($eng = 1; $eng < count($eIds); $eng++) {
                $engineer_where_clause .= ' OR spe.`ServiceProviderEngineerID` = ' . $eIds[$eng];
            }
        }

        $sqlVehicles = "
                SELECT 
			CONCAT(spe.`EngineerFirstName`,' ',`EngineerLastName`,'_$dayOfWeek') AS `name`
                FROM
                        `service_provider_engineer` spe
                WHERE
			spe.`ServiceProviderID` = $spId
        		AND (
				$engineer_where_clause
			)
               ";

        if ($this->debug)
            $this->controller->log("APIViamente::OptimiseRoute - Query sqlVehicles \n" . $sqlVehicles, 'viamente_api_');

        $result = $this->Query($this->conn, $sqlVehicles);

        if ($this->debug)
            $this->controller->log("APIAppointments::OptimiseRoute - Query sqlVehicles Result \n" . var_export($result, true), 'viamente_api_');

        $params = array();

        //$params['trafficFactor'] = floatval ($this->controller->config['Viamente']['TrafficFactor']);
        // BRS 114 - Traffic Factor now got from database rather than ini file - ajw 12/11/2012
        $params['trafficFactor'] = $this->getTrafficFactor($spId);              /* Get the Traffic Factor */

        for ($n = 0; $n < count($result); $n++) { /* Add availiable vehicles (engineers) to the paramter array */
            $params['vehicleNames'][$n] = $result[$n]['name'];
        }

        //if ($this->debug) $this->controller->log("APIViamente::OptimiseRoute - params = \n".var_export($params, true),'viamente_api_');
        if (!$notUnlockTmpEngs) {
            $this->processTempAssignedApps($spId, $date, 0);
        }
        $extraWhere = "";
        $key = $this->api_key;
        if ($skillType) {
            if ($skillType == "Brown") {
                $extraWhere = "and sk.RepairSkillID!=4";
            }
            if ($skillType == "White") {
                $extraWhere = "and sk.RepairSkillID=4";
                $key = $this->api_key2;
            }
        }
        if ($this->debug)
            $this->controller->log($key, 'viamente_api_key_');
        $sqlWayPoints = "
                        SELECT
                            IF (ISNULL(cu.`CustomerID`),
                                    CONCAT_WS(' ',nsj.`CustomerTitle`,nsj.`CustomerSurname`,a.`AppointmentID`)
                            ,
                                    CONCAT_WS(' ',cu.`ContactFirstName`,cu.`ContactLastName`,a.`AppointmentID`)
                            ) AS `name`,
                            IF (ISNULL(cu.`CustomerID`),
                                    nsj.`Postcode`
                            ,
                                    IF (ISNULL(j.`ColAddPostcode`) OR (j.`ColAddPostcode` = '') OR (j.`ColAddPostcode` = '0'),
                                            cu.`PostalCode`
                                    ,
                                            j.`ColAddPostcode`
                                    )
                            ) AS `location`,
                            a.`Latitude`,
                            a.`Longitude`,
                            a.`Duration` * 60 AS `serviceTimeSec`,
                            TIME_TO_SEC(a.`AppointmentStartTime`) AS `startTimeSec`,
                            TIME_TO_SEC(a.`AppointmentEndTime`) AS `stopTimeSec`,
                            TIME_TO_SEC(a.`AppointmentStartTime2`) AS `startTimeSec2`,
                            TIME_TO_SEC(a.`AppointmentEndTime2`) AS `stopTimeSec2`,
                            a.`AppointmentID`,
                            a.`importance`,
                            a.`ForceEngineerToViamente`,
                            CONCAT(spe.`EngineerFirstName`,' ',spe.`EngineerLastName`) AS `engname`
                        FROM
                            `appointment` a LEFT JOIN `job` j ON a.`jobID` = j.`JobID`
                                            LEFT JOIN `customer` cu ON j.`CustomerID` = cu.`CustomerID`
                                            LEFT JOIN `non_skyline_job` nsj ON a.`NonSkylineJobID` = nsj.`NonSkylineJobID`
                                            LEFT JOIN `service_provider_engineer` spe ON a.`ServiceProviderEngineerID`  = spe.`ServiceProviderEngineerID`
                                            join `service_provider_skillset` sps on sps.ServiceProviderSkillsetID=a.ServiceProviderSkillsetID
                                            join skillset sk on sk.SkillsetID=sps.SkillsetID
                        WHERE
                            a.`AppointmentDate` = '$date'
                            $extraWhere
			    AND a.`ServiceProviderID` = $spId
                            AND a.`ViamenteExclude` = 'No'
                            AND a.`CompletedBy` is null 
                        LIMIT 
                            300
            ";

        if ($this->debug)
            $this->controller->log("APIViamente::OptimiseRoute - Query sqlWayPoints \n" . $sqlWayPoints, 'viamente_api_');

        $resultWayPoint = $this->Query($this->conn, $sqlWayPoints);

        //echo "This page was created in ".$totaltime." seconds";
        $this->cleanOldValues($resultWayPoint);
        if ($this->debug)
            $this->controller->log("APIAppointments::RoutedAppointments - Query sqlWayPoints Result \n" . var_export($resultWayPoint, true), 'viamente_api_');

        $wp_idx = 0;
        for ($n = 0; $n < count($resultWayPoint); $n++) { /* Generate waypoint array in paramters for each required appointment */
            if (is_null($resultWayPoint[$n]['Latitude']) || is_null($resultWayPoint[$n]['Latitude'])) {
                if (is_null($resultWayPoint[$n]['location'])) {
                    $this->controller->log("No postcode can't optimise");
                    break;
                } else {
                    $location = $this->pc->getLatLong($resultWayPoint[$n]['location']);       /* Generate latitude and longitude from postcode */
                }
            } else {
                $location['latitude'] = $resultWayPoint[$n]['Latitude'];
                $location['longitude'] = $resultWayPoint[$n]['Longitude'];
            }
            if ($this->debug)
                $this->controller->log($location, 'geotag_api_');
            if (($location['latitude'] != -1 && $location['longitude'] != -1) && ($location['latitude'] != 0.0 && $location['longitude'] != 0.0)) {
                $params['waypoints'][$wp_idx] = array(
                    'name' => $resultWayPoint[$n]['name'],
                    'location' => $location,
                    'serviceTimeSec' => $resultWayPoint[$n]['serviceTimeSec'],
                    'priority' => $resultWayPoint[$n]['importance'],
                    'timeWindows' => array(array(
                            'startTimeSec' => $resultWayPoint[$n]['startTimeSec'],
                            'stopTimeSec' => intval($resultWayPoint[$n]['stopTimeSec']) - intval($resultWayPoint[$n]['serviceTimeSec'])
                    ))
                );

                if (!is_null($resultWayPoint[$n]['startTimeSec2']) /* Check if we have a second tiemslot for appointments */
                        && !is_null($resultWayPoint[$n]['stopTimeSec2'])) {
                    $params['waypoints'][$wp_idx]['timeWindows'][1]['startTimeSec'] = $resultWayPoint[$n]['startTimeSec2'];
                    $params['waypoints'][$wp_idx]['timeWindows'][1]['stopTimeSec'] = $resultWayPoint[$n]['stopTimeSec2'] - intval($resultWayPoint[$n]['serviceTimeSec']);
                }

                $sqlSkillSet = "
                                SELECT
                                        ss.`SkillsetName`			
                                FROM
                                        `appointment` a,
                                        `service_provider_skillset` sps,
                                        `skillset` ss
                                WHERE
                                        sps.`SkillsetID` = ss.`SkillsetID`
                                        AND a.`ServiceProviderSkillsetID` = sps.`ServiceProviderSkillsetID`
                                            AND a.`AppointmentID` = {$resultWayPoint[$n]['AppointmentID']}
                            ";

                if ($this->debug)
                    $this->controller->log("APIViamente::OptimiseRoute - Query sqlSkillSet [Appointment ID : {$resultWayPoint[$n]['AppointmentID']}] \n$sqlSkillSet", 'viamente_api_');

                $resultsSkillSet = $this->Query($this->conn, $sqlSkillSet);
                $resultWayPoint = $this->Query($this->conn, $sqlWayPoints);

                if ($this->debug)
                    $this->controller->log("APIAppointments::RoutedAppointments - Query sqlSkillSet Result [Appointment ID : {$resultWayPoint[$n]['AppointmentID']}] \n" . var_export($resultsSkillSet, true), 'viamente_api_');
                $f = 0;

                //if engineer is specified not adding required skillsets
                if ($resultWayPoint[$n]['ForceEngineerToViamente'] == 0) {
                    for ($f = 0; $f < count($resultsSkillSet); $f++) {
                        $params['waypoints'][$wp_idx]['tagsIncludeAnd'][$f] = $resultsSkillSet[$f]['SkillsetName'];
                    }
                }

                if ($resultWayPoint[$n]['ForceEngineerToViamente'] != 0) { /* Check if we are forcing Viamente to use a specific engineer */
                    $params['waypoints'][$wp_idx]['tagsIncludeAnd'][$f] = $resultWayPoint[$n]['engname'];
                } /* fi $resultWayPoint['ForceEngineerToViamente'] */


                //adding postcode as tag 
                //if engineer is specified not adding required postcodes
                if ($resultWayPoint[$n]['ForceEngineerToViamente'] == 0) {
                    if ($resultWayPoint[$n]['location'] != "") {//if postcode is blank (ireland or posibly error) do not add postcode tags
                        if ($lock[0]['KeepEngInPCArea'] == 'Yes') {
                            $postCode = $resultWayPoint[$n]['location'];


                            $postCode2 = trim(substr(trim($postCode), 0, -3));
                            $pcPart1 = $postCode2;
                            if (!is_numeric(substr($postCode2, -1))) {
                                $postCode2 = substr($postCode2, 0, strlen($postCode2) - 1);
                            }
                            $postCodeOutCode = $postCode2;             /* Extract the UK postcode outcode (eg CV1) from a full UK postcode (eg CV1 5FB) */
                            if (!is_numeric(substr($postCodeOutCode, -1))) {
                                $postCodeOutCode = substr($postCodeOutCode, 0, strlen($postCodeOutCode) - 1);
                            }
                            $postCodeArea = preg_replace('/[^\\/\-a-z\s]/i', '', $postCodeOutCode); /* Get the Post code Area (eg CV) */


                            $params['waypoints'][$wp_idx]['tagsIncludeOr'][0] = "$postCodeOutCode";
                            $params['waypoints'][$wp_idx]['tagsIncludeOr'][1] = "$postCodeArea";
                            $params['waypoints'][$wp_idx]['tagsIncludeOr'][2] = "$postCode";
                            $params['waypoints'][$wp_idx]['tagsIncludeOr'][3] = "$pcPart1";
                        }
                    }
                }
                //end psotcode as tag

                if (($params['waypoints'][$wp_idx]['location']['latitude'] == -1) || ($params['waypoints'][$wp_idx]['location']['longitude'] == -1)) {
                    unset($params['waypoints'][$wp_idx]);
                    $params['waypoints'] = array_values($params['waypoints']);
                    $postcodenotfound[] = $resultWayPoint[$n]['location'];
                } else {
                    $wp_idx++;
                } /* fi $params['waypoints'][$wp_idx]['location']['latitude'] || ['longitude']   */
            } else {
                $sql = "update appointment set Latitude=0.0 , Longitude=0.0 where AppointmentID=:AppointmentID";
                $param = ['AppointmentID' => $resultWayPoint[$n]['AppointmentID']];
                $this->Execute($this->conn, $sql, $param);
            }
        } /* fi is_null */

        $params_json = json_encode($params, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);

        if ($this->debug)
            $this->controller->log("APIViamente::OptimiseRoute - params = \n" . $params_json, 'viamente_api_');

        $resultWayPoint = $this->Query($this->conn, $sqlWayPoints);

        $matrix_name = $diary_matrix_model->GetName($spId, $date, $key);        /* Look to see if we have matrix name for this optimisation */
        if (is_null($matrix_name)) { /* If not */
            $mtx_create = $this->CreateMatrix($spId, $date, $key);              /* Create it */
            if (isset($mtx_create['name'])) { /* If we have a response */
                $matrix_name = $mtx_create['name'];                             /* use the matrix name */
            } /* fi isset $mtx_create['name'] */ else {
                $matrix_name = null;
            }
        } /* fi is_null $matrix_name */

        if (is_null($matrix_name)) { /* If we have a matrix name then use it for optimisation */
            $output = $this->viamenteCall($params_json, '/opt/v3/optimization', $key);
        } else {
            $output = $this->viamenteCall($params_json, '/opt/v3/optimization/name/' . urlencode($matrix_name), $key);
        }

        $resultWayPoint = $this->Query($this->conn, $sqlWayPoints);

        if ($this->debug)
            $this->controller->log("APIViamente::OptimiseRoute - REST Output \n" . var_export($output, true), 'viamente_api_');

        if ($output === false) {
            // Server failure
            $this->controller->log('REST Server Error: ' . var_export($output, true), 'viamente_api_');

            return(
                    array(
                        'Message' => 'REST Server Error'
                    )
                    );
        } else {
            $data = json_decode($output['response'], true);
            if ($this->debug)
                $this->controller->log("APIViamente::OptimiseRoute - REST Output \n" . var_export($data, true), 'viamente_api_');
            if (isset($data['routes']) && sizeof($data['routes']) > 0) { /* Success - array of routes */
                $resultWayPoint = $this->Query($this->conn, $sqlWayPoints);

                $routes = $data['routes'];

                $sql_update = " UPDATE appointment ";
                $sql_u1 = " SET `ServiceProviderEngineerID`= case AppointmentID ";
                $sql_u2 = " `ViamenteStartTime`= case AppointmentID ";
                $sql_u3 = " `ViamenteDepartTime`= case AppointmentID ";
                $sql_u4 = " `ViamenteReturnTime`= case AppointmentID ";
                $apids = "";
                foreach ($routes as $route) {
                    $vEngSplit = explode('_', $route['vehicleName']);            /* Engineer name needs to be split off from before _day */
                    $eId = $this->getEngineerIdViamente($vEngSplit[0], $spId);  /* Get the Engineer ID from the waypoint name */

                    if (!is_null($eId)) { /* Can't find appointment ID */
                        $steps = $route['steps'];

                        foreach ($steps as $step) {
                            if (isset($step['waypointName'])) {
                                $aId = $this->getAppointmentIdViamente($step['waypointName'], $date);  /* Get the appointment ID from the waypoint name */
                            } else {
                                $aId = null;
                            }

                            if (!is_null($aId)) {
                                $timeStartHours = intval($step['arrivalTimeSec'] / 60 / 60);
                                $timeStartMinutes = intval((($step['arrivalTimeSec'] / 60 / 60) - $timeStartHours) * 60);

                                $timeDepartHours = intval($step['departureTimeSec'] / 60 / 60);
                                $timeDepartMinutes = intval((($step['departureTimeSec'] / 60 / 60) - $timeDepartHours) * 60);

                                $timeNextArriveHours = intval(($step['departureTimeSec'] + $step['nextStepDriveTimeSec']) / 60 / 60);
                                $timeNextArriveMinutes = intval(((($step['departureTimeSec'] + $step['nextStepDriveTimeSec']) / 60 / 60) - $timeNextArriveHours) * 60);


                                $timeStart = date('H:i:s', mktime($timeStartHours, $timeStartMinutes, 0, 0, 0, 0)); //date as second argument uses timestamp :P
                                $timeDepart = date('H:i:s', mktime($timeDepartHours, $timeDepartMinutes, 0, 0, 0, 0)); //date as second argument uses timestamp :P
                                $timeArive = date('H:i:s', mktime($timeNextArriveHours, $timeNextArriveMinutes, 0, 0, 0, 0)); //date as second argument uses timestamp :P


                                $vStart = $date . ' ' . $timeStart;
                                $vDepart = $date . ' ' . $timeDepart;
                                $vArrive = $date . ' ' . $timeArive;
                                $apids.=$aId . ",";
                                $sql_u1.="  
                                    WHEN $aId THEN $eId ";
                                $sql_u2.=" 
                                    WHEN $aId THEN '$vStart' 
                                  ";
                                $sql_u3.=" 
                                    WHEN $aId THEN '$vDepart' 
                                  ";
                                $sql_u4.=" 
                                    WHEN $aId THEN '$vArrive' 
                                  ";

                                // if ($this->debug) $this->controller->log("Update appoints table\ncmd = \n$cmd\nf_appointment = \n".var_export($f_appointment,true),'viamente_api_');
                                // $this->Execute($this->conn, $cmd, $f_appointment);
                            } /* fi isnull $aId */
                        } /* next $step */
                    } else { /* Successfully got AppointmentID */
                        $this->controller->log("Can't find engineer id for {$vEngSplit[0]} on $date", 'viamente_api_');
                    } /* fi isnull $eId */
                } /* next $route */
                $sql_u1.=" end, ";
                $sql_u2.=" end, ";
                $sql_u3.=" end, ";
                $sql_u4.=" end ";
                $sql_update.=$sql_u1 . $sql_u2 . $sql_u3 . $sql_u4;
                $sql_update.="  where AppointmentID in ($apids 0)";
                $this->Execute($this->conn, $sql_update);
            } else { /* Request failed */
                $this->controller->log("Viamente call failed\noutput=\n" . var_export($output, true), 'viamente_api_');
            }
        }
        $resultWayPoint = $this->Query($this->conn, $sqlWayPoints);









        $response = json_decode($output['response']);

        /* if (count($postcodenotfound) > 0 ) {
          for ($p = 0; $p < count($postcodenotfound); $p++) {
          $response['PostCodeNotFound'][$p] = $postcodenotfound[$p];
          }
          } */
        if ($this->debug)
            $this->controller->log("APIViamente::OptimiseRoute - END --------------------", 'viamente_api_');
        $resultWayPoint = $this->Query($this->conn, $sqlWayPoints);





        if (!$notUnlockTmpEngs) {

            $this->processTempAssignedApps($spId, $date, 1); //lock all engs in temp assigned apps
        }

        return($response);
    }

    /**
     * getAppointmentIdViamente
     *  
     * Get the appointment Id based upon waypoint data returned from Viamente
     * Assumption waypoint name is unique for date
     * 
     * @param array $name   Waypoint name
     *              $date   Date of appointment
     * 
     * @return assoicative array with appointment details or none
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function getAppointmentIdViamente($name, $date) {
        $name = str_replace("'", "\'", $name);

        $sql = "
                SELECT
			a.`AppointmentID`
		FROM
			`appointment` a LEFT JOIN `job` j ON a.`JobID` = j.`JobID`
			                LEFT JOIN `customer` cu ON j.`CustomerID` = cu.`CustomerID`
			                LEFT JOIN `non_skyline_job` nsj ON a.`NonSkylineJobID` = nsj.`NonSkylineJobID`
		WHERE
			a.`AppointmentDate` = '$date'
			AND IF (ISNULL(a.`JobID`),
				CONCAT_WS(' ',nsj.`CustomerTitle`,nsj.`CustomerSurname`,a.`AppointmentID`) = '$name'
			,
				CONCAT_WS(' ',cu.`ContactFirstName`,cu.`ContactLastName`,a.`AppointmentID`) = '$name'
			)
                    
               ";

        $result = $this->Query($this->conn, $sql);

        if (count($result) > 0) {
            return($result[0]['AppointmentID']);                                /* Appointment exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }

    /**
     * getDiaryAllocationId
     *  
     * Get the DiaryAllocationId of an appointment
     * 
     * @param array $aId    Appointment ID
     * 
     * @return assoicative array with appointment details or none
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function getDiaryAllocationId($aId) {
        $sql = "
                SELECT
			a.`DiaryAllocationID`
		FROM
			`appointment` a 
		WHERE
			a.`AppointmentID` = $aId
               ";

        $result = $this->Query($this->conn, $sql);

        if (count($result) > 0) {
            return($result[0]['DiaryAllocationID']);                            /* Diary Allocation exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }

    /**
     * getEngineerIdViamente
     *  
     * Get the engineer Id based upon waypoint data returned from Viamente
     * Assumption engineer name is unique for service provider
     * 
     * @param array $name   Waypoint name
     *              $spId   Service Provider ID
     * 
     * @return assoicative array with appointment details or none
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function getEngineerIdViamente($name, $spId) {
        $sql = "
                SELECT
			`ServiceProviderEngineerID`
		FROM
			`service_provider_engineer`
		WHERE
			CONCAT(`EngineerFirstName`,' ',`EngineerLastName`) = '$name'
			AND `ServiceProviderID` = $spId
               ";

        if ($this->debug)
            $this->controller->log("APIViamente::getEngineerIdViamente - END sql = $sql", 'viamente_api_');

        $result = $this->Query($this->conn, $sql);

        if ($this->debug)
            $this->controller->log("APIViamente::getEngineerIdViamente - END result = " . var_export($result, true), 'viamente_api_');

        if (count($result) > 0) {
            return($result[0]['ServiceProviderEngineerID']);                    /* Engineer exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }

    /**
     * updateAppointment
     *  
     * Update an appointment
     * 
     * @param array $args   Associative array of field values for to update the
     *                      appointment. The array must include the primary key
     *                      AppointmentID
     * 
     * @return integer  Number of rows effected
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function updateAppointment($args) {
        $cmd = $this->table_appointment->updateCommand($args);

        if ($this->Execute($this->conn, $cmd, $args)) {
            $result = array(/* Job successfully created */
                'status' => 'SUCCESS',
                'message' => ''
            );
        } else {
            $result = array(
                'status' => 'FAIL',
                'message' => $this->lastPDOError() /* Return the error */
            );
        }
        return $result;
    }

    /**
     * updateDiaryAllocation
     *  
     * Update teh diray allocation
     * 
     * @param array $args
     * 
     * @return integer  Number of rows effected
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function updateDiaryAllocation($args) {
        $cmd = $this->table_diary_allocation->updateCommand($args);

        if ($this->Execute($this->conn, $cmd, $args)) {
            $result = array(/* Job successfully created */
                'status' => 'SUCCESS',
                'message' => ''
            );
        } else {
            $result = array(
                'status' => 'FAIL',
                'message' => $this->lastPDOError() /* Return the error */
            );
        }
        return $result;
    }

    public function viamenteCall($data, $url, $key, $method = 'POST') {
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json'
        );
        $handle = curl_init();

        if ($this->debug)
            $this->controller->log($this->controller->config['Viamente']['ViamenteApiUrl'] . $url . "?key=$key", 'viamente_api_');

        curl_setopt($handle, CURLOPT_URL, $this->controller->config['Viamente']['ViamenteApiUrl'] . $url . "?key=$key");
        curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($handle, CURLOPT_TIMEOUT, 78);

        switch ($method) {
            case 'GET':
                break;

            case 'POST':
                curl_setopt($handle, CURLOPT_POST, true);
                curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
                break;

            case 'PUT':
                curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
                break;

            case 'DELETE':
                curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
        }
        $exectime = microtime();
        $exectime = explode(" ", $exectime);
        $exectime = $exectime[1] + $exectime[0];
        $starttime = $exectime;

        $response = curl_exec($handle);
        $exectime = microtime();
        $exectime = explode(" ", $exectime);
        $exectime = $exectime[1] + $exectime[0];
        $endtime = $exectime;
        $totaltime = ($endtime - $starttime);
        $this->controller->log("pahese:3(curl exec end) =" . $totaltime, "viamneteOptimise_speed______");
        $info = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        curl_close($handle);

        //$this->log(var_export($response,true),'viamente_api_');

        $raw = $response;                                                       /* Store response, (used in some specific API logs) */
        /* $this->log(var_export($info,true),'viamente_api_'); */
        if (is_array($info)) {
            if (stripos($info['content_type'], 'json') !== false) {
                $response = json_decode($response, true);
            } else if (stripos($info['content_type'], 'xml') !== false) {
                $response = preg_replace("/[^\x0A\x0D\x20-\x7E]/", "", $response);
                $response = (array) simplexml_load_string($response);
            }
        }

        return array('response' => $response,
            'info' => $info,
            'raw' => $raw);
    }

    /**
     * cleanOldValues
     *  
     * Remove old optimised values
     * 
     * @param array $r
     * 
     * @return nothing
     * 
     * @author Andris Polnikovs <a.polnikovs@pccsuk.com>  
     * ************************************************************************ */
    public function cleanOldValues($r) {


        $sql_update = "update appointment set ServiceProviderEngineerID=null,ViamenteTravelTime=null,ViamenteDepartTime=null,ViamenteStartTime=null,ViamenteUnreached=0,ViamenteUnreachedTimestamp=null  ";
        $sqlIds = "";
        foreach ($r as $e) {
            $id = $e['AppointmentID'];
            $sqlIds.="$id,";
        }
        $sql_update.="where AppointmentID in ($sqlIds 0) and ForceEngineerToViamente='0' ";
        $this->execute($this->conn, $sql_update);
        $sql2 = "update appointment set ViamenteStartTime=null,ViamenteUnreached=0,ViamenteUnreachedTimestamp=null,ViamenteTravelTime=null,ViamenteDepartTime=null  where AppointmentID in ($sqlIds 0) and ForceEngineerToViamente='1' ";
        $this->execute($this->conn, $sql2);
    }

    /**
     * getViamenteKey
     *  
     * Get the Viamente key for a service provider
     * 
     * @param inter $spId   The ID of the service provider we want the key for
     * 
     * @return string   The Viamenete key
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    private function getViamenteKey($spId) {
        $sql = "
                SELECT
			`ViamenteKey`
		FROM
			`service_provider`
		WHERE
			`ServiceProviderID` = $spId
               ";

        $result = $this->Query($this->conn, $sql);

        if (count($result) > 0) {
            return($result[0]['ViamenteKey']);                                  /* Viamente exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }

    /**
     * getTrafficFactor
     *  
     * Get the traffic factor for a service provider
     * 
     * @param inter $spId   The ID of the service provider we want the key for
     * 
     * @return string   The Viamenete key
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    private function getTrafficFactor($spId) {
        $sql = "
                SELECT
			`DefaultTravelSpeed`
		FROM
			`service_provider`
		WHERE
			`ServiceProviderID` = $spId
               ";

        $result = $this->Query($this->conn, $sql);

        if (count($result) > 0) { /* Check Service Proviser exists */
            $retval = floatval($result[0]['DefaultTravelSpeed'] / 100);         /* Speed factor stored as percentage so convert to decimal - eg 120 % = 1.2 */
            $retval = 1 / $retval;                                              /* TraffiFactor is 1 / speed factor */

            return($retval);
        } else {
            return(null);                                                       /* Not found return null */
        }
    }

    /**
     * OptimiseRouteIndividualEngineers
     *  
     * Produce an optimised route
     * 
     * @param $spId     The ID of the service provider
     *        $date     The date rqequired
     *        $eIds     Array of engineers required
     * 
     * @return Array containing output of the API call
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function OptimiseRouteIndividualEngineers($spId, $date, $eIds) {
        $sql = "select sp.KeepEngInPCArea from service_provider sp where sp.ServiceProviderID=$spId"; //checking if sp is set to lock engineers postcodes areas
        $lock = $this->Query($this->conn, $sql);

        if ($this->debug)
            $this->controller->log("APIViamente::OptimiseRouteIndividualEngineers - START --------------------", 'viamente_api_');

        $postcodenotfound = array();

        $dayOfWeek = date('l', strtotime($date));

        $engineer_where_clause = 'spe.`ServiceProviderEngineerID` = ' . $eIds[0];
        if (count($eIds) > 1) {
            for ($eng = 1; $eng < count($eIds); $eng++) {
                $engineer_where_clause .= ' OR spe.`ServiceProviderEngineerID` = ' . $eIds[$eng];
            }
        }

        $sqlVehicles = "
                SELECT 
			CONCAT(spe.`EngineerFirstName`,' ',`EngineerLastName`,'_$dayOfWeek') AS `name`,
                            spe.PrimarySkill
                FROM
                        `service_provider_engineer` spe
                WHERE
			spe.`ServiceProviderID` = $spId
        		AND (
				$engineer_where_clause
			)
               ";

        if ($this->debug)
            $this->controller->log("APIViamente::OptimiseRouteIndividualEngineers - Query sqlVehicles \n" . $sqlVehicles, 'viamente_api_');

        $result = $this->Query($this->conn, $sqlVehicles);

        if ($this->debug)
            $this->controller->log("APIViamente::OptimiseRouteIndividualEngineers - Query sqlVehicles Result \n" . var_export($result, true), 'viamente_api_');

        $params = array();

        //$params['trafficFactor'] = floatval ($this->controller->config['Viamente']['TrafficFactor']);
        // BRS 114 - Traffic Factor now got from database rather than ini file - ajw 12/11/2012
        $params['trafficFactor'] = $this->getTrafficFactor($spId);              /* Get the Traffic Factor */

        for ($n = 0; $n < count($result); $n++) { /* Add availiable vehicles (engineers) to the paramter array */
            $params['vehicleNames'][$n] = $result[$n]['name'];
        }

        //if ($this->debug) $this->controller->log("APIViamente::OptimiseRoute - params = \n".var_export($params, true),'viamente_api_');
        $key = $this->api_key;
        $extraWhere = "";
        if ($spId == 64) {

            $extraWhere = "and sk.RepairSkillID!=4";


            if ($result[0]['PrimarySkill'] == "White Goods") {
                $key = $this->api_key2;
                $extraWhere = "and sk.RepairSkillID=4";
            }
        }
        if ($this->debug)
            $this->controller->log($key, 'viamente_api_key_');
        $sqlWayPoints = "
                        SELECT
                            IF (ISNULL(cu.`CustomerID`),
                                    CONCAT_WS(' ',nsj.`CustomerTitle`,nsj.`CustomerSurname`,a.`AppointmentID`)
                            ,
                                    CONCAT_WS(' ',cu.`ContactFirstName`,cu.`ContactLastName`,a.`AppointmentID`)
                            ) AS `name`,
                            IF (ISNULL(cu.`CustomerID`),
                                    nsj.`Postcode`
                            ,
                                    IF (ISNULL(j.`ColAddPostcode`) OR (j.`ColAddPostcode` = '') OR (j.`ColAddPostcode` = '0'),
                                            cu.`PostalCode`
                                    ,
                                            j.`ColAddPostcode`
                                    )
                            ) AS `location`,
                            a.`Duration` * 60 AS `serviceTimeSec`,
                            TIME_TO_SEC(a.`AppointmentStartTime`) AS `startTimeSec`,
                            TIME_TO_SEC(a.`AppointmentEndTime`) AS `stopTimeSec`,
                            TIME_TO_SEC(a.`AppointmentStartTime2`) AS `startTimeSec2`,
                            TIME_TO_SEC(a.`AppointmentEndTime2`) AS `stopTimeSec2`,
                            a.`AppointmentID`,
                            a.`importance`,
                            a.`ForceEngineerToViamente`,
                            CONCAT(spe.`EngineerFirstName`,' ',spe.`EngineerLastName`) AS `engname`
                        FROM
                            `appointment` a LEFT JOIN `job` j ON a.`jobID` = j.`JobID`
                                            LEFT JOIN `customer` cu ON j.`CustomerID` = cu.`CustomerID`
                                            LEFT JOIN `non_skyline_job` nsj ON a.`NonSkylineJobID` = nsj.`NonSkylineJobID`
                                            LEFT JOIN `service_provider_engineer` spe ON a.`ServiceProviderEngineerID`  = spe.`ServiceProviderEngineerID`
                                             join `service_provider_skillset` sps on sps.ServiceProviderSkillsetID=a.ServiceProviderSkillsetID
                                             join skillset sk on sk.SkillsetID=sps.SkillsetID
                                             
                                           
                            
                        WHERE
                          
			     a.`AppointmentDate` = '$date'
			    AND a.`ServiceProviderID` = $spId
                               $extraWhere
                            AND (
				$engineer_where_clause
                                OR a.`ServiceProviderEngineerID` IS NULL
                        	OR a.`ServiceProviderEngineerID` = 0
			    ) and a.ViamenteExclude='No' and a.CompletedBy is null
            ";

        if ($this->debug)
            $this->controller->log("APIViamente::OptimiseRouteIndividualEngineers  - Query sqlWayPoints \n" . $sqlWayPoints, 'viamente_api_');

        $resultWayPoint = $this->Query($this->conn, $sqlWayPoints);
        $this->cleanOldValues($resultWayPoint);
        if ($this->debug)
            $this->controller->log("APIAppointments::OptimiseRouteIndividualEngineers  - Query sqlWayPoints Result \n" . var_export($resultWayPoint, true), 'viamente_api_');

        $wp_idx = 0;
        for ($n = 0; $n < count($resultWayPoint); $n++) { /* Generate waypoint array in paramters for each required appointment */
            if (is_null($resultWayPoint[$n]['location'])) {
                $this->controller->log("No postcode can't optimise");
            } else {
                $params['waypoints'][$wp_idx] = array(
                    'name' => $resultWayPoint[$n]['name'],
                    'location' => $this->pc->getLatLong($resultWayPoint[$n]['location']), /* Generate latitude and longitude from postcode */
                    'serviceTimeSec' => $resultWayPoint[$n]['serviceTimeSec'],
                    'priority' => $resultWayPoint[$n]['importance'],
                    'timeWindows' => array(array(
                            'startTimeSec' => $resultWayPoint[$n]['startTimeSec'],
                            'stopTimeSec' => intval($resultWayPoint[$n]['stopTimeSec']) - intval($resultWayPoint[$n]['serviceTimeSec'])
                    ))
                );

                if (!is_null($resultWayPoint[$n]['startTimeSec2']) /* Check if we have a second tiemslot for appointments */
                        && !is_null($resultWayPoint[$n]['stopTimeSec2'])) {
                    $params['waypoints'][$wp_idx]['timeWindows'][1]['startTimeSec'] = $resultWayPoint[$n]['startTimeSec2'];
                    $params['waypoints'][$wp_idx]['timeWindows'][1]['stopTimeSec'] = $resultWayPoint[$n]['stopTimeSec2'];
                }

                $sqlSkillSet = "
                                SELECT
                                        ss.`SkillsetName`			
                                FROM
                                        `appointment` a,
                                        `service_provider_skillset` sps,
                                        `skillset` ss
                                WHERE
                                        sps.`SkillsetID` = ss.`SkillsetID`
                                        AND a.`ServiceProviderSkillsetID` = sps.`ServiceProviderSkillsetID`
                                            AND a.`AppointmentID` = {$resultWayPoint[$n]['AppointmentID']}
                            ";

                if ($this->debug)
                    $this->controller->log("APIViamente::OptimiseRouteIndividualEngineerse - Query sqlSkillSet [Appointment ID : {$resultWayPoint[$n]['AppointmentID']}] \n$sqlSkillSet", 'viamente_api_');

                $resultsSkillSet = $this->Query($this->conn, $sqlSkillSet);

                if ($this->debug)
                    $this->controller->log("APIAppointments::OptimiseRouteIndividualEngineers - Query sqlSkillSet Result [Appointment ID : {$resultWayPoint[$n]['AppointmentID']}] \n" . var_export($resultsSkillSet, true), 'viamente_api_');



                //if engineer is specified not adding required skillsets
                if ($resultWayPoint[$n]['ForceEngineerToViamente'] == 0) {

                    for ($f = 0; $f < count($resultsSkillSet); $f++) {
                        $params['waypoints'][$wp_idx]['tagsIncludeAnd'][$f] = $resultsSkillSet[$f]['SkillsetName'];
                    }
                }


                if ($resultWayPoint[$n]['ForceEngineerToViamente'] != 0 && $resultWayPoint[$n]['ForceEngineerToViamente'] != "") { /* Check if we are forcing Viamente to use a specific engineer */
                    $params['waypoints'][$wp_idx]['tagsIncludeAnd'][$f] = $resultWayPoint[$n]['engname'];
                } /* fi $resultWayPoint['ForceEngineerToViamente'] */

                //if engineer is specified not adding required postcodes
                if ($resultWayPoint[$n]['ForceEngineerToViamente'] == 0) {

                    //adding postcode as tag 
                    if ($resultWayPoint[$n]['location'] != "") {//if postcode is blank (ireland or posibly error) do not add postcode tags
                        if ($lock[0]['KeepEngInPCArea'] == 'Yes') {
                            $postCode = $resultWayPoint[$n]['location'];


                            $postCode2 = trim(substr(trim($postCode), 0, -3));
                            $pcPart1 = $postCode2;
                            if (!is_numeric(substr($postCode2, -1))) {
                                $postCode2 = substr($postCode2, 0, strlen($postCode2) - 1);
                            }
                            $postCodeOutCode = $postCode2;             /* Extract the UK postcode outcode (eg CV1) from a full UK postcode (eg CV1 5FB) */
                            if (!is_numeric(substr($postCodeOutCode, -1))) {
                                $postCodeOutCode = substr($postCodeOutCode, 0, strlen($postCodeOutCode) - 1);
                            }
                            $postCodeArea = preg_replace('/[^\\/\-a-z\s]/i', '', $postCodeOutCode); /* Get the Post code Area (eg CV) */


                            $params['waypoints'][$wp_idx]['tagsIncludeOr'][0] = "$postCodeOutCode";
                            $params['waypoints'][$wp_idx]['tagsIncludeOr'][1] = "$postCodeArea";
                            $params['waypoints'][$wp_idx]['tagsIncludeOr'][2] = "$postCode";
                            $params['waypoints'][$wp_idx]['tagsIncludeOr'][3] = "$pcPart1";
                        }
                    }
                }
                //end psotcode as tag







                if (($params['waypoints'][$wp_idx]['location']['latitude'] == -1) || ($params['waypoints'][$wp_idx]['location']['longitude'] == -1)) {
                    unset($params['waypoints'][$wp_idx]);
                    $params['waypoints'] = array_values($params['waypoints']);
                    $postcodenotfound[] = $resultWayPoint[$n]['location'];
                } else {
                    $wp_idx++;
                } /* fi $params['waypoints'][$wp_idx]['location']['latitude'] || ['longitude']   */
            }
        } /* fi is_null */

        $params_json = json_encode($params, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
        if ($this->debug)
            $this->controller->log("APIViamente::OptimiseRouteIndividualEngineers - params = \n" . $params_json, 'viamente_api_');

        $output = $this->viamenteCall($params_json, '/opt/v3/optimization', $key);

        if ($this->debug)
            $this->controller->log("APIViamente::OptimiseRouteIndividualEngineers - REST Output \n" . var_export($output, true), 'viamente_api_');

        if ($output === false) {
            // Server failure
            $this->controller->log('REST Server Error: ' . var_export($output, true), 'viamente_api_');

            return(
                    array(
                        'Message' => 'REST Server Error'
                    )
                    );
        } else {
            $data = json_decode($output['response'], true);
            if ($this->debug)
                $this->controller->log("APIViamente::OptimiseRouteIndividualEngineers - REST Output \n" . var_export($data, true), 'viamente_api_');
            if (isset($data['routes'])) {

                /* Success - array of routes */
                $routes = $data['routes'];

                foreach ($routes as $route) {
                    $vEngSplit = explode('_', $route['vehicleName']);            /* Engineer name needs to be split off from before _day */
                    $eId = $this->getEngineerIdViamente($vEngSplit[0], $spId);     /* Get the Engineer ID from the waypoint name */

                    if (!is_null($eId)) { /* Can't find appointment ID */
                        $steps = $route['steps'];

                        foreach ($steps as $step) {
                            if (isset($step['waypointName'])) {
                                $aId = $this->getAppointmentIdViamente($step['waypointName'], $date);  /* Get the appointment ID from the waypoint name */
                            } else {
                                $aId = null;
                            }

                            if (!is_null($aId)) {
                                $timeStartHours = intval($step['arrivalTimeSec'] / 60 / 60);
                                $timeStartMinutes = intval((($step['arrivalTimeSec'] / 60 / 60) - $timeStartHours) * 60);

                                $timeDepartHours = intval($step['departureTimeSec'] / 60 / 60);
                                $timeDepartMinutes = intval((($step['departureTimeSec'] / 60 / 60) - $timeDepartHours) * 60);

                                $timeNextArriveHours = intval(($step['departureTimeSec'] + $step['nextStepDriveTimeSec']) / 60 / 60);
                                $timeNextArriveMinutes = intval(((($step['departureTimeSec'] + $step['nextStepDriveTimeSec']) / 60 / 60) - $timeNextArriveHours) * 60);

                                $timeTravelHours = intval(($step['nextStepDriveTimeSec']) / 60 / 60);
                                $timeTravelMinutes = intval(((($step['nextStepDriveTimeSec']) / 60 / 60) - $timeNextArriveHours) * 60);


                                $timeStart = date('H:i:s', mktime($timeStartHours, $timeStartMinutes, 0, 0, 0, 0)); //date as second argument uses timestamp : APolnikovs
                                $timeDepart = date('H:i:s', mktime($timeDepartHours, $timeDepartMinutes, 0, 0, 0, 0)); //date as second argument uses timestamp :APolnikovs
                                $timeArive = date('H:i:s', mktime($timeNextArriveHours, $timeNextArriveMinutes, 0, 0, 0, 0)); //date as second argument uses timestamp :APolnikovs
                                $timeTravel = date('H:i:s', mktime($timeTravelHours, $timeTravelMinutes, 0, 0, 0, 0)); //date as second argument uses timestamp :APolnikovs

                                $timeStart = date('H:i:s', mktime($timeStartHours, $timeStartMinutes, 0, 0, 0, 0)); //date as second argument uses timestamp :APolnikovs

                                $f_appointment = array(
                                    'AppointmentID' => $aId,
                                    'ServiceProviderEngineerID' => $eId,
                                    'ViamenteStartTime' => $date . ' ' . $timeStart,
                                    'ViamenteDepartTime' => $date . ' ' . $timeDepart,
                                    'ViamenteReturnTime' => $date . ' ' . $timeArive,
                                    'ViamenteTravelTime' => $date . ' ' . $timeTravel
                                );

                                $cmd = $this->table_appointment->updateCommand($f_appointment);
                                if ($this->debug)
                                    $this->controller->log("Update appoints table\ncmd = \n$cmd\nf_appointment = \n" . var_export($f_appointment, true), 'update_viamenteInfo_');
                                $this->Execute($this->conn, $cmd, $f_appointment);
                            } /* fi isnull $aId */
                        } /* next $step */
                    } else { /* Successfully got AppointmentID */
                        $this->controller->log("Can't find engineer id for {$route['vehicleName']} on $date", 'viamente_api_');
                    } /* fi isnull $eId */
                } /* next $route */
            } else { /* Request failed */
                $this->controller->log("Viamente call failed\noutput=\n" . var_export($output, true), 'viamente_api_');
            }
        }

        $response = json_decode($output['response']);

        /* if (count($postcodenotfound) > 0 ) {
          for ($p = 0; $p < count($postcodenotfound); $p++) {
          $response['PostCodeNotFound'][$p] = $postcodenotfound[$p];
          }
          } */
        if ($this->debug)
            $this->controller->log("APIViamente::OptimiseRouteIndividualEngineers - END --------------------", 'viamente_api_');
        return($response);
    }

    /**
     * ProcessEngineer
     *  
     * Create a new or update an existing engineer (vehicle) in the Viamente system
     * 
     * @param integer $spId     ID of service rpovider
     * @param date $date        Date of the service provider
     * @param string $key       The key used
     * 
     * We must now pass the key as Andris has meade some chanegs so that there 
     * are two keys per service centre.
     * 
     * @return Array containing output of the API call
     * 
     * @author Andrew Williams <andrew.williams@awcomputech.com>  
     * ************************************************************************ */
    public function CreateMatrix($spId, $date, $key) {
        $diary_matrix_model = $this->controller->LoadModel('DiaryMatrix');
        $service_providers_model = $this->controller->LoadModel('ServiceProviders');

        $sp = $service_providers_model->fetchRow(array('ServiceProviderID' => $spId));

        $name = urlencode("{$sp['CompanyName']}_$date");                        /* Generate the name we will use for this matrix */

        if ($this->debug)
            $this->controller->log("APIViamente::CreateMatrix - START --------------------", 'viamente_api_');

        $output = $this->viamenteCall('', '/opt/v3/matrix', $key . "&name=$name", 'POST');     /* Call viamente to create matrix */

        if ($this->debug)
            $this->controller->log("APIViamente::CreateMatrix - API output" . var_export($output, true), 'viamente_api_');

        $data = json_decode($output['response'], true);                         /* Get the Viamente output */
        if (isset($data['id'])) {
            $matrix = array(/* Build fields for diary_matrix table */
                'ViamenteMatrixID' => $data['id'],
                'Name' => $data['name'],
                'Date' => $date,
                'ServiceProviderID' => $spId,
                'Key' => $key
            );

            $dmc = $diary_matrix_model->create($matrix);                            /* Create diary_matrix entry */

            if ($this->debug)
                $this->controller->log("APIViamente::CreateMatrix - diary_matrix create  output" . var_export($dmc, true), 'viamente_api_');

            if ($this->debug)
                $this->controller->log("APIViamente::CreateMatrix - END --------------------", 'viamente_api_');
            return($data);
        }else {
            return null;
        }
    }

    /**
     * getGeoTagFromAddress
     *  
     * ??
     * 
     * @param ?? $add   ??
     * 
     * @return ??
     * 
     * @author Andris Polnikovs <a.polnikovs@pccsuk.com> 
     * ************************************************************************ */
    public function getGeoTagFromAddress($add) {
        $address = str_replace(" ", "+", $add);
        // echo"$add";
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json'
        );
        $handle = curl_init();

        curl_setopt($handle, CURLOPT_URL, "https://maps.googleapis.com/maps/api/geocode/json?address=$address+uk&sensor=false");
        curl_setopt($handle, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)");
        curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($handle, CURLOPT_TIMEOUT, 360);


        $data = curl_exec($handle);
        $info = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        curl_close($handle);
        $data = json_decode($data, true);
        if (isset($data["results"][0]["geometry"]["location"])) {
            $geotag['lat'] = $data["results"][0]["geometry"]["location"]["lat"];
            $geotag['lng'] = $data["results"][0]["geometry"]["location"]["lng"];

            return $geotag;
        } else {
            return FALSE;
        }
    }

    /**
     * processTempAssignedApps
     *  
     * this function is dealing with temporary assigned engineers
     * 
     * @param integer $spID     Service provider ID
     * @param date $date        ??
     * @param ? $mode           ??
     * 
     * @return ??
     * 
     * @author Andris Polnikovs <a.polnikovs@pccsuk.com> 
     * ************************************************************************ */
    public function processTempAssignedApps($spID, $date, $mode) {
        //mode 1 lock appointments
        //mode 2 unlock appointments

        $sql = "update appointment a set a.ForceEngineerToViamente=$mode 
        where a.ServiceProviderID=$spID 
        and a.AppointmentDate=:AppointmentDate
        and a.TempSpecified='Yes'";
        $params = array('AppointmentDate' => $date);
        $this->Execute($this->conn, $sql, $params);
    }

}

?>
