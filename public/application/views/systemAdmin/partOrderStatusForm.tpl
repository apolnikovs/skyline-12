<script type="text/javascript">
$(document).ready(function() {
    $("#ServiceProviderID").combobox();
});
</script>  
    
    <div id="PartOrderStatusFormPanel" class="SystemAdminFormPanel" >
    
                <form id="PartOrderStatusForm" name="PartOrderStatusForm" method="post"  action="{$_subdomain}/LookupTables/savePartOrderStatus" class="inline" >
       
                <fieldset>
                    <legend title="" >Part Order Status</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

                            </p>
                       
                            <p>
                            <label class="cardLabel" for="ServiceProviderID" >Service Provider:</label>
                            &nbsp;&nbsp; 
                                <select name="ServiceProviderID" id="ServiceProviderID" class="text" >
                                <option value="" {if $datarow.ServiceProviderID eq ''}selected="selected"{/if}></option>

                                
                                {foreach $ServiceProviders as $c}

                                    <option value="{$c.ServiceProviderID}" {if $datarow.ServiceProviderID eq $c.ServiceProviderID}selected="selected"{/if}>{$c.CompanyName|escape:'html'}</option>
                                    
                                {/foreach}
                                
                            </select>
                                
                        </p>
                       
                         
                         
                         <p>
                            <label class="cardLabel" for="PartStatusName" >Status Name:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="PartStatusName" value="{$datarow.PartStatusName|escape:'html'}" id="PartStatusName" >
                        
                         </p>
                         <p>
                            <label class="cardLabel" for="PartStatusDescription" >Description:</label>
                           
                            &nbsp;&nbsp; <textarea   style="width:310px;resize:auto;height:100px;"  name="PartStatusDescription" id="PartStatusDescription" >{$datarow.PartStatusDescription|escape:'html'}</textarea>
                        
                         </p>
                         <p>
                             <label style="width:70%"  class="cardLabel" for="DisplayOrderSection" >Display Order Section when inserting parts:</label>
                           
                             &nbsp;&nbsp; <input style="float: right;margin-right: 135px;"  type="checkbox"   name="DisplayOrderSection" {if $datarow.DisplayOrderSection=="Yes"}checked=checked{/if} id="DisplayOrderSection" >
                        
                       </p>
                       <p>
                           <label style="width:70%" class="cardLabel" for="Status" >In-active:</label>
                           
                             &nbsp;&nbsp; <input  style="float: right;margin-right: 135px;" type="checkbox"   name="Status" {if $datarow.Status=="In-active"}checked=checked{/if} id="Status" >
                        </p>
                         </p>
                         
                         
                         
                         <input type="hidden" name="PartStatusID" value="{$datarow.PartStatusID}">
                     

                         <p>
                <hr>
                               
                                <div style="height:20px;margin-bottom: 10px">
                                <button type="submit" style="float: left" class="gplus-blue">Save</button>
                                <button type="button" onclick="$.colorbox.close();" style="float:right" class="gplus-blue">Cancel</button>
                                </div>
            </p>    

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
 
                          
                        
