{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
    <script type="text/javascript" >
        
        function addFields(selected) {	
                    var selected_split = selected.split(","); 	


                    $("#BuildingNameNumber").val(selected_split[0]).blur();	
                    $("#Street").val(selected_split[1]).blur();
                    $("#LocalArea").val(selected_split[2]).blur();
                    $("#TownCity").val(selected_split[3]).blur();
                    $("#selectOutput").slideUp("slow"); 

                }
    function setCountryBasedOnCounty($countyId, $countryId, $parentCountryId)
    {
        $($countyId+" option:selected").each(function () {
            $selected_cc_id =  $($countyId+" option:selected").attr('id');
            $country_id_array = $selected_cc_id.split('_');
            if($country_id_array[1]!='0')
            {
                $($countryId).val($country_id_array[1]);
                setValue($countryId);
            }
            else
            {
                 $($parentCountryId+" input:first").val('');
            }
        });
    }            
    $(document).ready(function() {
        $("#CountyID").combobox({
            change: function() {
                setCountryBasedOnCounty("#CountyID", "#CountryID", "#P_CountryID");
            }
        });
        $("#CountryID").combobox({
            change: function() {
                var options = '';
                var selectedCountryRates = vat_rate_array[$('#CountryID').val()];
                $('#VATRateID').empty()
                $('#VATRateID').append('<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>');
                for (var n = 0; n < selectedCountryRates.length; n++) {
                    if (selectedCountryRates[n] !== undefined) {
                    options += '<option value="' + n + '">' + selectedCountryRates[n] + '</option>';
                    }
                }
                $('#VATRateID').append(options);
            }
        });
        $('#VATRateID').combobox();
              
             function autoHint()
             {
                 $('.auto-hint').each(function() {
                                    $this = $(this);
                                    if ($this.val() == $this.attr('title')) {
                                        $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                        if ($this.hasClass('auto-pwd')) {
                                            $this.prop('type','password');
                                        }
                                    }
                        } );  
             }
             
             
             
             $(document).on('click', '#insert_save_btn', 
                                function() {
                                
                                autoHint();
                
               });
               
             $(document).on('click', '#update_save_btn', 
                                function() {
                                
                                autoHint();
                
               });
                
                
                
            /* =======================================================
            *
            * Initialise input auto-hint functions...
            *
            * ======================================================= */

            $('.auto-hint').focus(function() {
                    $this = $(this);
                    if ($this.val() == $this.attr('title')) {
                        $this.val('').removeClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','password');
                        }
                    }
                } ).blur(function() {
                    $this = $(this);
                    if ($this.val() == '' && $this.attr('title') != '')  {
                        $this.val($this.attr('title')).addClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','text');
                        }
                    }         
                } ).each(function(){
                    $this = $(this);
                    if ($this.attr('title') == '') { return; }
                    if ($this.val() == '') { 
                        if ($this.attr('type') == 'password') {
                            $this.addClass('auto-pwd').prop('type','text');
                        }
                        $this.val($this.attr('title')); 
                    } else { 
                        $this.removeClass('auto-hint'); 
                    }
                    $this.attr('autocomplete','off');
                } ); 
                
                
                
                
         });       
        
    </script>
    
    
    <div id="SNFormPanel" class="SystemAdminFormPanel" >
    
                <form id="SNForm" name="SNForm" method="post"  action="#" class="inline">
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            
                            <p>
                                <label ></label>
                                <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>
                            </p>
                       
                         
                         
                         <p>
                            <label class="fieldLabel" for="CompanyName" >{$page['Labels']['company_name']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="CompanyName" value="{$datarow.CompanyName|escape:'html'}" id="CompanyName" >
                        
                         </p>
                         
                         
                        <p>
                            <label class="fieldLabel" for="PostalCode">{$page['Labels']['postcode']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <input  class="text uppercaseText" style="width: 140px;" type="text" name="PostalCode" id="PostalCode" value="{$datarow.PostalCode|escape:'html'}" >&nbsp;

                            <input type="submit" name="quick_find_btn" id="quick_find_btn" class="textSubmitButton postCodeLookUpBtn"  onclick="return false;"   value="Click to find Address" >

                            &nbsp;&nbsp;<span id="fetch" style="display: none" class="blueText" >({$page['Text']['fetching']|escape:'html'})</span>

                        </p>

                        <p id="selectOutput" ></p>


                        <p>
                            <label class="fieldLabel" for="BuildingNameNumber" >{$page['Labels']['building_name']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text"  name="BuildingNameNumber" value="{$datarow.BuildingNameNumber|escape:'html'}" id="BuildingNameNumber" >          
                        </p>

                        <p>
                            <label class="fieldLabel" for="Street" >{$page['Labels']['street']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <input type="text" class="text"  name="Street" value="{$datarow.Street|escape:'html'}" id="Street" >          
                        </p>

                        <p>
                            <label class="fieldLabel" for="LocalArea" >{$page['Labels']['area']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="LocalArea" value="{$datarow.LocalArea|escape:'html'}" id="LocalArea" >          
                        </p>


                        <p>
                            <label class="fieldLabel" for="TownCity" >{$page['Labels']['city']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <input type="text" class="text" name="TownCity" value="{$datarow.TownCity|escape:'html'}" id="TownCity" >          
                        </p>

                        <p>
                            <label class="fieldLabel" for="County" >{$page['Labels']['county']|escape:'html'}:</label>
                            &nbsp;&nbsp; 


                            <select name="CountyID" id="CountyID" class="text" >
                                <option value="" id="country_0_county_0" {if $datarow.CountyID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $countries as $country}
                                    {foreach $country.Counties as $county}
                                    <option value="{$county.CountyID}"  id="country_{$country.CountryID}_county_{$county.CountyID}" {if $datarow.CountyID eq $county.CountyID}selected="selected"{/if}>{$county.Name|escape:'html'}</option>
                                    {/foreach}
                                {/foreach}

                            </select>

                        </p>
                         
                        
                        
                         <p id="P_CountryID">
                            <label class="fieldLabel" for="CountryID" >{$page['Labels']['country']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; 
                                <select name="CountryID" id="CountryID" >
                                <option value="" {if $datarow.CountryID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $countries as $country}

                                    <option value="{$country.CountryID}" {if $datarow.CountryID eq $country.CountryID}selected="selected"{/if}>{$country.Name|escape:'html'}</option>

                                {/foreach}

                            </select>
                        </p>
                        
                         
                        <p>
                                <label class="fieldLabel" for="ContactEmail">{$page['Labels']['email']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; <input  type="text" name="ContactEmail" class="text auto-hint" title="{$page['Text']['email']|escape:'html'}" value="{$datarow.ContactEmail|escape:'html'}" id="ContactEmail" >
                                <br>
                        </p>

                        <p>
                            <label class="fieldLabel" for="ContactPhone" >{$page['Labels']['phone']|escape:'html'}:</label>
                            &nbsp;&nbsp; 
			    <input  type="text" class="text phoneField"  name="ContactPhone" value="{$datarow.ContactPhone|escape:'html'}" id="ContactPhone" >
                            <input type="text" class="text extField auto-hint" title="{$page['Text']['extension_no']|escape:'html'}" name="ContactPhoneExt" value="{$datarow.ContactPhoneExt|escape:'html'}" id="ContactPhoneExt" >

                        </p>
                        
			
			<p>
			    <hr>
			</p>
		
			{*
			<p>
			    <span style="width:155px; display:inline-block; margin-left:220px; padding-bottom:0px; text-align:center;">{$page['Labels']['forename']|escape:'html'}</span>
			    <span style="width:155px; display:inline-block; padding-bottom:0px; text-align:center;">{$page['Labels']['surname']|escape:'html'}</span>
			</p>
			*}
			{*
			<p>
			    <span style="width:312px; display:inline-block; margin-left:220px; padding-bottom:0px; text-align:center;">{$page['Labels']['email']|escape:'html'}</span>
			</p>
			*}
			
			{* Service Manager *}
			<p>
			    <label class="fieldLabel" for="ServiceManagerForename">{$page['Labels']['service_manager']|escape:'html'}:</label>
			    &nbsp;&nbsp;
			    <input type="text" name="ServiceManagerForename" placeholder="Forename" style="width:142px;" class="text" title="" value="{$datarow.ServiceManagerForename|escape:'html'}" id="ServiceManagerForename">
			    <input type="text" name="ServiceManagerSurname" placeholder="Surname" style="width:142px;" class="text" title="" value="{$datarow.ServiceManagerSurname|escape:'html'}" id="ServiceManagerSurname">
			    <br>
                        </p>
			
			<p>
			    <label class="fieldLabel" for="ServiceManagerEmail">{$page['Labels']['service_manager_email']|escape:'html'}:</label>
			    &nbsp;&nbsp;
			    <input type="text" name="ServiceManagerEmail" class="text" title="" value="{$datarow.ServiceManagerEmail|escape:'html'}" id="ServiceManagerEmail">
			    <br>
                        </p>
			
			{* Admin Supervisor *}
			<p>
			    <label class="fieldLabel" for="AdminSupervisorForename">{$page['Labels']['admin_supervisor']|escape:'html'}:</label>
			    &nbsp;&nbsp;
			    <input type="text" name="AdminSupervisorForename" placeholder="Forename" style="width: 142px;" class="text" title="" value="{$datarow.AdminSupervisorForename|escape:'html'}" id="AdminSupervisorForename">
			    <input type="text" name="AdminSupervisorSurname" placeholder="Surname" style="width: 142px;" class="text" title="" value="{$datarow.AdminSupervisorSurname|escape:'html'}" id="AdminSupervisorSurname">
			    <br>
                        </p>
			<p>
			    <label class="fieldLabel" for="AdminSupervisorEmail">{$page['Labels']['admin_supervisor_email']|escape:'html'}:</label>
			    &nbsp;&nbsp;
			    <input type="text" name="AdminSupervisorEmail" class="text" title="" value="{$datarow.AdminSupervisorEmail|escape:'html'}" id="AdminSupervisorEmail">
			    <br>
                        </p>
			
			{* Network Manager *}
			<p>
			    <label class="fieldLabel" for="NetworkManagerForename">{$page['Labels']['network_manager']|escape:'html'}:</label>
			    &nbsp;&nbsp;
			    <input type="text" name="NetworkManagerForename" placeholder="Forename" style="width: 142px;" class="text" title="" value="{$datarow.NetworkManagerForename|escape:'html'}" id="NetworkManagerForename">
			    <input type="text" name="NetworkManagerSurname" placeholder="Surname" style="width: 142px;" class="text" title="" value="{$datarow.NetworkManagerSurname|escape:'html'}" id="NetworkManagerSurname">
			    <br>
                        </p>
			<p>
			    <label class="fieldLabel" for="NetworkManagerEmail">{$page['Labels']['network_manager_email']|escape:'html'}:</label>
			    &nbsp;&nbsp;
			    <input type="text" name="NetworkManagerEmail" class="text" title="" value="{$datarow.NetworkManagerEmail|escape:'html'}" id="NetworkManagerEmail">
			    <br>
                        </p>
			
			{* ASC Daily Report Send Yes/No *}
			<p>
			    <label class="fieldLabel" for="SendASCReport">{$page['Labels']['send_reports']|escape:'html'}:</label>
			    &nbsp;&nbsp; 
			    <input type="radio" name="SendASCReport" value="Yes" {if $datarow.SendASCReport == "Yes"}checked="checked"{/if} /><span class="text">Yes</span> 
			    <input type="radio" name="SendASCReport" value="No" {if $datarow.SendASCReport == "No"}checked="checked"{/if} /><span class="text">No</span> 
			</p>
			
			{* Network Daily Report Send Yes/No *}
			<p>
			    <label class="fieldLabel" for="SendNetworkReport">{$page['Labels']['send_network_report']|escape:'html'}:</label>
			    &nbsp;&nbsp; 
			    <input type="radio" name="SendNetworkReport" value="Yes" {if $datarow.SendNetworkReport == "Yes"}checked="checked"{/if} /><span class="text">Yes</span> 
			    <input type="radio" name="SendNetworkReport" value="No" {if $datarow.SendNetworkReport == "No"}checked="checked"{/if} /><span class="text">No</span> 
			</p>
			
			<p>
			   <hr>
			</p>
                        
                        
                         
                         <p>
                            <label class="fieldLabel" for="CreditPrice" >{$page['Labels']['credit_price']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="CreditPrice" value="{$datarow.CreditPrice|escape:'html'}" id="CreditPrice" >
                        
                         </p>
                         
                         
                         <p>
                            <label class="fieldLabel" for="CreditLevel" >{$page['Labels']['credit_level']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="CreditLevel" value="{$datarow.CreditLevel|escape:'html'}" id="CreditLevel" >
                        
                         </p>
                         
                         <p>
                            <label class="fieldLabel" for="ReorderLevel" >{$page['Labels']['reorder_level']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="ReorderLevel" value="{$datarow.ReorderLevel|escape:'html'}" id="ReorderLevel" >
                        
                         </p>
                         
                         
                         <p>
                            <hr>
                         </p>
                         
                         
                          <p>
                            <label class="fieldLabel" for="InvoiceCompanyName" >{$page['Labels']['invoice_to_company_name']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="InvoiceCompanyName" value="{$datarow.InvoiceCompanyName|escape:'html'}" id="InvoiceCompanyName" >
                        
                          </p>
                         
                          <p>
                            <label class="fieldLabel" for="VATRateID" >{$page['Labels']['vat_code']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; 
                             
                            <select name="VATRateID" id="VATRateID" >
                            <option value="" {if $datarow.VATRateID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                            {if $datarow.CountryID && isset($vatRates[$datarow.CountryID])}
                                {foreach $vatRates[$datarow.CountryID] as $vatRate}

                                    <option value="{$vatRate.VatRateID}" {if $datarow.VATRateID eq $vatRate.VatRateID}selected="selected"{/if}>{$vatRate.Code|escape:'html'}</option>

                                {/foreach}
                             {/if}
                            </select>
                          </p>
                          
                          <p>
                            <label class="fieldLabel" for="VATNumber" >{$page['Labels']['vat_number']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="VATNumber" value="{$datarow.VATNumber|escape:'html'}" id="VATNumber" >
                        
                          </p>
                         
                          <p>
                            <label class="fieldLabel" for="SageReferralNominalCode" >{$page['Labels']['sage_referral_nominal_code']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="SageReferralNominalCode" value="{$datarow.SageReferralNominalCode|escape:'html'}" id="SageReferralNominalCode" >
                        
                          </p>
                         
                          <p>
                            <label class="fieldLabel" for="SageLabourNominalCode" >{$page['Labels']['sage_labour_nominal_code']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="SageLabourNominalCode" value="{$datarow.SageLabourNominalCode|escape:'html'}" id="SageLabourNominalCode" >
                        
                          </p>
                         
                          <p>
                            <label class="fieldLabel" for="SagePartsNominalCode" >{$page['Labels']['sage_parts_nominal_code']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="SagePartsNominalCode" value="{$datarow.SagePartsNominalCode|escape:'html'}" id="SagePartsNominalCode" >
                        
                          </p>
                          
                          <p>
                            <label class="fieldLabel" for="SageCarriageNominalCode" >{$page['Labels']['sage_carriage_nominal_code']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="SageCarriageNominalCode" value="{$datarow.SageCarriageNominalCode|escape:'html'}" id="SageCarriageNominalCode" >
                        
                          </p>

                          <p>
                            <hr>
                          </p>  

                          
                          <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                {foreach $statuses as $status}

                                    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                                {/foreach}    
                          </p>
                         
                         

                            <p>
                    
                                <span class= "bottomButtons" >

                                    <input type="hidden" name="NetworkID"  value="{$datarow.NetworkID|escape:'html'}" >
                                 
                                    {if $datarow.NetworkID neq '' && $datarow.NetworkID neq '0'}
                                        
                                         <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                    
                                    {else}
                                        
                                        <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                        
                                    {/if}
                                   
                                        <br>
                                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                                </span>

                            </p>

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                                             
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
