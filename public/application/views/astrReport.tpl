{extends "DemoLayout.tpl"}

{block name=config}

{$PageId = $SiteMapPage}
{/block}

{block name=scripts}

{*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    
{/block}

{block name=body}

    
<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">{$page['Text']['home_page']}</a> /<a href="{$_subdomain}/index/index/siteMap">{$page['Text']['page_title']}</a> / ASTR Report
    </div>
</div>
    
{include file='include/menu.tpl'}

        





<div id="sitemapContainer" class="wrapper">

	<div id="left-panel">

	    <div id="astrLeftDiv" class="corners gradient">
	    <h3 class="blue-letters-large" style="padding:7px 0px 0px 23px;">ASTR Check Factor Alerts</h3>


		<table id="astrTable">
		    
		    <tr>
			<td class="astrTableFirstTd">CF1</td>
			<td class="astrTableEmptyTd"></td>
			<td>
			    All calls must be logged onto Service Suppliers systems within 1 day-Jobs are electronically comunicated 
			    from Samsung, therefore score should always be 100%.
			</td>
			<td class="astrTableEmptyTd"></td>
			<td class="astrTableLastTd">0</td>
		    </tr>
		    
		    <tr>
			<td class="astrTableFirstTd">CF2</td>
			<td class="astrTableEmptyTd"></td>
			<td>
			    The appointment or estimated repair date must be created on the Service Suppliers systems within 2 hours
			    of the job being received at the service supplier.
			</td>
			<td class="astrTableEmptyTd"></td>
			<td class="astrTableLastTd">2</td>
		    </tr>
		    
		    <tr>
			<td class="astrTableFirstTd">CF3</td>
			<td class="astrTableEmptyTd"></td>
			<td>
			    The service supplier must assign an Engineer (status set to Engineer Assigned -09) within 1 working day.
			</td>
			<td class="astrTableEmptyTd"></td>
			<td class="astrTableLastTd">6</td>
		    </tr>

		    <tr>
			<td class="astrTableFirstTd">CF5</td>
			<td class="astrTableEmptyTd"></td>
			<td>
			    If the appointment or estimated repair date is changed a reason must be supplied within 1 working day.
			</td>
			<td class="astrTableEmptyTd"></td>
			<td class="astrTableLastTd">5</td>
		    </tr>

		    <tr>
			<td class="astrTableFirstTd">CF7</td>
			<td class="astrTableEmptyTd"></td>
			<td>
			    The Complete date must be with 1 working day of the final appointment date.
			</td>
			<td class="astrTableEmptyTd"></td>
			<td class="astrTableLastTd">7</td>
		    </tr>

		    <tr>
			<td class="astrTableFirstTd">CF8</td>
			<td class="astrTableEmptyTd"></td>
			<td>
			    The system must be updated with the Repair Completion date be within 3 working days of the actual Job
			    Completion date.
			</td>
			<td class="astrTableEmptyTd"></td>
			<td class="astrTableLastTd">4</td>
		    </tr>

		    <tr>
			<td class="astrTableFirstTd">CF9</td>
			<td class="astrTableEmptyTd"></td>
			<td>
			    To maintain data integrity the SVC dates must match the Warranty Claim Date. In particular, the Received
			    Date and the Completed Date must match the warranty claim.
			</td>
			<td class="astrTableEmptyTd"></td>
			<td class="astrTableLastTd">3</td>
		    </tr>
		    
		</table>
		    
	    </div>
	    
	</div>
    
    
	<div id="right-panel" class="corners gradient">
		<h3 class="blue-letters-large" style="padding:7px 0px 0px 23px;">CF1 Call Logged Analysis</h3>
		<div class="sidebar-nav overflow">  
            <ul class="nav nav-list">  
              <!--li class="nav-header"></li-->  
              <li><a href="#">1. Birmingham Specialised Services<span class="label label-important" style="float:right;">360</span></a></li>  
              <li><a href="#">2. JF Associates<span class="label label-warning" style="float:right;">280</span></a></li>  
              <li><a href="#">3. DBS York<span class="label label-success" style="float:right;">115</span></a></li>  
              <li><a href="#">4. DK Audio Visuals<span class="label label-important" style="float:right;">360</span></a></li>  
              <li><a href="#">5. Sontec<span class="label label-warning" style="float:right;">280</span></a></li>  
              <li><a href="#">6. DSC Solutions<span class="label label-warning" style="float:right;">280</span></a></li>  
              <li><a href="#">7. Startec<span class="label label-success" style="float:right;">115</span></a></a></li>  
              <li><a href="#">8. Genserve<span class="label label-success" style="float:right;">115</span></a></a></li>  
              <li><a href="#">9. Visual FX Service Centre<span class="label label-important" style="float:right;">360</span></a></li>  
              <li><a href="#">10. DBS York<span class="label label-success" style="float:right;">115</span></a></a></li>  
              <li><a href="#">11. JF Associates<span class="label label-success" style="float:right;">115</span></a></li>  
              <li><a href="#">12. Startec<span class="label label-success" style="float:right;">115</span></a></li>
              <li><a href="#">1. Birmingham Specialised Services<span class="label label-success" style="float:right;">115</span></a></li>  
              <li><a href="#">2. JF Associates<span class="label label-warning" style="float:right;">280</span></a></li>  
              <li><a href="#">3. DBS York<span class="label label-success" style="float:right;">115</span></a></li>  
              <li><a href="#">4. DK Audio Visuals<span class="label label-important" style="float:right;">360</span></a></li>  
              <li><a href="#">5. Sontec<span class="label label-success" style="float:right;">115</span></a></a></li>  
              <li><a href="#">6. DSC Solutions<span class="label label-success" style="float:right;">115</span></a></li>  
              <li><a href="#">7. Startec<span class="label label-success" style="float:right;">115</span></a></li>  
              <li><a href="#">8. Genserve<span class="label label-success" style="float:right;">115</span></a></a></li>  
              <li><a href="#">9. Visual FX Service Centre<span class="label label-important" style="float:right;">360</span></a></li>  
              <li><a href="#">10. DBS York<span class="label label-success" style="float:right;">115</span></a></a></li>  
              <li><a href="#">11. JF Associates<span class="label label-success" style="float:right;">115</span></a></li>  
              <li><a href="#">12. Startec<span class="label label-success" style="float:right;">115</span></a></li>
              <li><a href="#">2. JF Associates<span class="label label-important" style="float:right;">360</span></a></li>  
              <li><a href="#">3. DBS York<span class="label label-warning" style="float:right;">280</span></a></li>  
              <li><a href="#">4. DK Audio Visuals<span class="label label-important" style="float:right;">360</span></a></li>  
              <li><a href="#">5. Sontec<span class="label label-important" style="float:right;">360</span></a></li>  
              <li><a href="#">6. DSC Solutions<span class="label label-warning" style="float:right;">280</span></a></li>  
              <li><a href="#">8. Genserve<span class="label label-important" style="float:right;">360</span></a></li>  
              <li><a href="#">9. Visual FX Service Centre<span class="label label-success" style="float:right;">115</span></a></li>  
              <li><a href="#">10. DBS York<span class="label label-success" style="float:right;">115</span></a></a></li>  
              <li><a href="#">11. JF Associates<span class="label label-success" style="float:right;">115</span></a></a></li>  
              <li><a href="#">12. Startec<span class="label label-success" style="float:right;">115</span></a></a></li>
              <li><a href="#">1. Birmingham Specialised Services<span class="label label-success" style="float:right;">115</span></a></li>  
              <li><a href="#">2. JF Associates<span class="label label-success" style="float:right;">115</span></a></li>  
              <li><a href="#">3. DBS York<span class="label label-warning" style="float:right;">280</span></a></li>  
              <li><a href="#">4. DK Audio Visuals<span class="label label-success" style="float:right;">115</span></a></li>  
              <li><a href="#">5. Sontec<span class="label label-success" style="float:right;">115</span></a></a></li>  
              <li><a href="#">6. DSC Solutions<span class="label label-success" style="float:right;">115</span></a></li>  
              <li><a href="#">8. Genserve<span class="label label-success" style="float:right;">115</span></a></li>  
              <li><a href="#">9. Visual FX Service Centre<span class="label label-warning" style="float:right;">280</span></a></li>  
              <li><a href="#">10. DBS York<span class="label label-important" style="float:right;">360</span></a></li>  
              <li><a href="#">11. JF Associates<span class="label label-important" style="float:right;">360</span></a></li>  
              <li><a href="#">12. Startec<span class="label label-important" style="float:right;">360</span></a></li>    
           </ul> 
          </div>
	</div>
</div>





{/block}
