{extends "DemoLayout.tpl"}


    {block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $ServiceTypes}
    {/block}
    {block name=afterJqueryUI}
        <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
        <style type="text/css" >
            .ui-combobox-input {
                 width:300px;
             }  
        </style>
    {/block}
    {block name=scripts}



    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}


    <script type="text/javascript">
        
    var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
                    
    var $jobTypes = new Array();
       
        {foreach from=$allBrandsJobTypes item=bJobTypes key=brandId}
           
           $jobTypes["{$brandId}"] = [        
            
            {foreach from=$bJobTypes item=jTypes} 
                   ["{$jTypes.JobTypeID}", "{$jTypes.Type}"],
            {/foreach}
                
           ]
           
        {/foreach}
                          
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    function inactiveRow(nRow, aData)
    {
          
        if (aData[5]==$statuses[1][1])
        {  
            $(nRow).addClass("inactive");

            $('td:eq(4)', nRow).html( $statuses[1][0] );
        }
        else
        {
            $(nRow).addClass("");
                  $('td:eq(4)', nRow).html( $statuses[0][0] );
        }
    }
    
   

    function gotoInsertPage($sRow)
    {
       
        $("#copyRowId").val($sRow[0]);
        $('#addButtonId').trigger('click');
        $("#copyRowId").val('');
        
        
    }

 

    $(document).ready(function() {



                

                  //change handler for brand dropdown box.
                  /*$(document).on('change', '#BrandID', 
                                function() {
                                    
                                  
                                    $jobTypesDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';
                                
                                    var $brandId = $("#BrandID").val();
                                
                                    if($brandId && $brandId!='')
                                        {
                                            $brandJobTypes = $jobTypes[$brandId];
                                            if($brandJobTypes)
                                                {
                                                    for(var $i=0;$i<$brandJobTypes.length;$i++)
                                                    {
                                                        $jobTypesDropDownList += '<option value="'+$brandJobTypes[$i][0]+'" >'+$brandJobTypes[$i][1]+'</option>';
                                                    }
                                                }
                                                
                                           $("#JobTypeID").html($jobTypesDropDownList);     
                                        }
                                    

                                });*/  
                    



                  //Click handler for finish button.
                  $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/lookupTables');

                                });







                   /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        



                     {if $SupderAdmin eq true} 

                        var  displayButtons = "UPA";
                     
                     {else}

                         var displayButtons =  "UP";

                     {/if} 
                     


                    
                    $('#STResults').PCCSDataTable( {

			"aoColumns": [ 
			    /* ServiceTypeID	*/  { "bVisible":   false },    
			    /* ServiceType ID	*/  { "sWidth" :    "110px" },
			    /* ServiceTypeName	*/  { "sWidth" :    "170px" },
			    /* Code		*/  null,
			    /* JobTypeID	*/  null,
			    /* Status		*/  null,
			    /* Brand		*/  null,
			    /* Brand ID		*/  null
			],
                            
                            
                            
                            
                            
                               
                            displayButtons:  displayButtons,
                            addButtonId:     'addButtonId',
                            addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
                            createFormTitle: '{$page['Text']['insert_page_legend']|escape:'html'}',
                            createAppUrl:    '{$_subdomain}/LookupTables/serviceTypes/insert/',
                            createDataUrl:   '{$_subdomain}/LookupTables/ProcessData/ServiceTypes/',
                            formInsertButton:'insert_save_btn',
                            copyInsertRowId: 'copyRowId',
                            
                            frmErrorRules:   {
                                            
                                                    BrandID:
                                                        {
                                                            required: true
                                                        },
                                                    ServiceTypeName:
                                                        {
                                                            required: true
                                                        },  
                                                    Code:
                                                        {
                                                            required: true
                                                        },
                                                    JobTypeID:
                                                        {
                                                            required: true
                                                        },  
                                                    Priority:
                                                        {
                                                            required: true,
                                                            digits: true
                                                        }, 
                                                    Status:
                                                        {
                                                            required: true
                                                        }     
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                    BrandID:
                                                        {
                                                            required: "{$page['Text']['brand_error']|escape:'html'}"
                                                        },
                                                    ServiceTypeName:
                                                        {
                                                            required: "{$page['Text']['name_error']|escape:'html'}"
                                                        },  
                                                    Code:
                                                        {
                                                            required: "{$page['Text']['code_error']|escape:'html'}"
                                                        },
                                                    JobTypeID:
                                                        {
                                                            required: "{$page['Text']['job_type_error']|escape:'html'}"
                                                        },    
                                                   Priority:
                                                        {
                                                            required: "{$page['Errors']['priority']|escape:'html'}"
                                                        },     
                                                    Status:
                                                        {
                                                            required: "{$page['Text']['status_error']|escape:'html'}"
                                                        }    
                                              },                     
                            
                            popUpFormWidth:  715,
                            popUpFormHeight: 430,
                            
                            pickButtonId:"copy_btn",
                            pickButtonText:"{$page['Buttons']['copy']|escape:'html'}",
                            pickCallbackMethod: "gotoInsertPage",
                            colorboxForceClose: false,
                            
                            
                            
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/LookupTables/serviceTypes/update/',
                            updateDataUrl:   '{$_subdomain}/LookupTables/ProcessData/ServiceTypes/',
                            formUpdateButton:'update_save_btn',
                            
                            colorboxFormId:  "STForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'STResultsPanel',
                            htmlTableId:     'STResults',
                            fetchDataUrl:    '{$_subdomain}/LookupTables/ProcessData/ServiceTypes/fetch/',
                            formCancelButton:'cancel_btn',
                           // pickCallbackMethod: 'openJob',
                            dblclickCallbackMethod: 'gotoEditPage',
                            fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError"


                        });
                      



                   

    });

    </script>

    {/block}

    {block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/lookupTables" >{$page['Text']['lookup_tables']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="LTTopPanel" >
                    <form id="countyForm" name="countyForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="LTResultsPanel" id="STResultsPanel" >

                    <table id="STResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
                                <tr>
                                        <th></th> 
                                        <th title="{$page['Text']['service_type_id_no']|escape:'html'}" >{$page['Text']['service_type_id_no']|escape:'html'}</th>
                                        <th title="{$page['Text']['name']|escape:'html'}" >{$page['Text']['name']|escape:'html'}</th>
                                        <th title="{$page['Text']['code']|escape:'html'}"  >{$page['Text']['code']|escape:'html'}</th>
                                        <th title="{$page['Text']['job_type']|escape:'html'}"  >{$page['Text']['job_type']|escape:'html'}</th>
                                        <th title="{$page['Text']['status']|escape:'html'}" >{$page['Text']['status']|escape:'html'}</th>
                                        <th title="{$page['Text']['brand']|escape:'html'}" >{$page['Text']['brand']|escape:'html'}</th>
                                        <th title="{$page['Text']['brand_id']|escape:'html'}" >{$page['Text']['brand_id']|escape:'html'}</th>
                                </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>  
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                </div>        


                <input type="hidden" name="copyRowId" id="copyRowId" > 

                {if $SupderAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               


    </div>
                        
                        



{/block}



