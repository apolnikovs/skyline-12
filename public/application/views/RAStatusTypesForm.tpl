{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel">   
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post" action="#" class="inline">
	    <fieldset>
		<legend title="">{$page['Text']['error_page_legend']|escape:'html'}</legend>
		<p>
		    <label class="formCommonError">{$accessDeniedMsg|escape:'html'}</label><br/><br/>
		</p>
		<p>
		    <span class= "bottomButtons" >
			<input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;" value="{$page['Buttons']['ok']|escape:'html'}" />
		    </span>
		</p>
	    </fieldset>   
	</form>            
    </div>    
    
{else}  
    
    <script src="{$_subdomain}/js/jquery.multiselect.js" type="text/javascript"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <link href="{$_subdomain}/css/themes/pccs/jquery.multiselect.css" rel="stylesheet" type="text/css" />
   
    <script type="text/javascript">   
     
        $(document).ready(function() {
	
            $("#BrandID").combobox();
            $("#AuthorisationType").combobox();
            $("#ServiceBaseStatus").combobox();
	    $("#Colour").jPicker({
		window: {
		    expandable:	true,
		    position: {
			x: "screenCenter", // acceptable values "left", "center", "right", "screenCenter", or relative px value
			y: "150px" // acceptable values "top", "bottom", "center", or relative px value
		    }
		}
	    });

	    $(".Color").css({ width: "25px", height: "24px", padding: "0px" });


	    var showCheckedOnly = false;

            $("#Subset").multiselect({
		create: function(event, ui) {
		    
		},
		open: function(event, ui) {
		    $(".ui-widget-header").css("height", "40px");
		    if (!$("#checkedDiv").length) {
			var html = "<div id='checkedDiv' style='display:block; position:relative; float:left; margin-top:3px;'>\
					<input type='radio' value='checked' name='showCheckedOnly' /> Checked only\
					<input type='radio' value='all' name='showCheckedOnly' /> All\
				    </div>\
				   ";
			$(html).appendTo(".ui-widget-header");
		    }
		    if(showCheckedOnly) {
			$("input[name=showCheckedOnly][value=checked]").attr("checked", true);
			hideUnchecked();
		    } else {
			$("input[name=showCheckedOnly][value=all]").attr("checked", true);
		    }
		}
	    });
	    
	    $(document).on("change", "input[name=showCheckedOnly]", function() {
		if($(this).val() == "all") {
		    showCheckedOnly = false;
		    showAll();
		} else {
		    showCheckedOnly = true;
		    hideUnchecked();
		}
	    });
	    
	    function hideUnchecked() {
		$(".ui-multiselect-checkboxes li label input").each(function() {
		    if(!$(this).is(":checked")) {
			$(this).parent().parent().hide();
		    }
		});
	    }
	    
	    function showAll() {
		$(".ui-multiselect-checkboxes li").each(function() {
		    $(this).show();
		});
	    }
	    
	    
	    function reloadStatusTypes() {
		$.post(
		    "{$_subdomain}/LookupTables/getRAStatusTypes", 
		    { 
			brandID:    $("#BrandID").val(),
			statusCode: $($(".row_selected").children()[0]).text()
		    }, 
		    function(response) {
			var html;
			var data = $.parseJSON(response);
			for(var i = 0; i<data.types.length; i++) {
			    html += "<option value='" + data.types[i].RAStatusID + "' " + (($.inArray(data.types[i].RAStatusID, data.subset) != -1) ? "selected='selected'" : "") + ">" + data.types[i].RAStatusName + "</option>";
			}
			$("#Subset").html(html);
			$("#Subset").multiselect("refresh");
		    }
		);
	    }
	    
	    reloadStatusTypes();
		
	    /*$(document).on("change", "#BrandID", function(data) {
		reloadStatusTypes();
	    });*/
	    /*
	    if(copy) {
		$("#Subset, .ui-multiselect").hide();
		$("<p style='width:300px; display:inline-block; margin-left:10px;'>If you need to specify a subset you must save this record first.</p>").insertAfter("label[for=Subset]");
	    }*/
	    
	}); 

    </script> 
    
    
    <div id="RASTFormPanel" class="SystemAdminFormPanel">
    
	<form id="RASTForm" name="RASTForm" method="post" action="#" class="inline">
       
	    <fieldset>
		
		<legend title="">{$form_legend|escape:'html'}</legend>
                        
		<p><label id="suggestText"></label></p>
		<p>
		    <label></label>
		    <span class="topText">
			{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}
		    </span>
		</p>
        
		<p>
		    <label class="fieldLabel" for="BrandID" >{$page['Labels']['brand']|escape:'html'}:<sup>*</sup></label>
		    &nbsp;&nbsp;
		    <select name="BrandID" id="BrandID">
			<option value="" {if $datarow.BrandID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>
			{foreach $brands as $brand}
			    <option value="{$brand.BrandID|escape:'html'}" {if $datarow.BrandID eq $brand.BrandID} selected="selected" {/if}>{$brand.Name|escape:'html'} ({$brand.BrandID|escape:'html'}) </option>
			{/foreach}
		    </select>
		</p>
                
		{if isset($insert) && $insert}
		    <p>
			<label class="fieldLabel" for="AuthorisationType">{$page['Labels']['auth_type']|escape:'html'}:<sup>*</sup></label>
			&nbsp;&nbsp;
			<select name="AuthorisationType" id="AuthorisationType">
			    <option value="">Select Authorisation Type</option>
			    {foreach $authTypes as $type}
				<option value="{$type.AuthorisationTypeID|escape:'html'}">{$type.AuthorisationTypeName|escape:'html'}</option>
			    {/foreach}
			</select>
		    </p>
		{else}
		    <p>
			<label class="fieldLabel" for="AuthorisationType">{$page['Labels']['auth_type']|escape:'html'}:<sup>*</sup></label>
			&nbsp;&nbsp;
			<span style="margin-top:4px; display:inline-block;">{$datarow.AuthorisationTypeName}</span>
		    </p>
		{/if}
                
		{if isset($copy) && $copy}
		    <input type="hidden" value="{$datarow.AuthorisationTypeID}" name="AuthorisationType" />
		{/if}
		
		<p>
		    <label class="fieldLabel" for="ServiceBaseStatus">{$page['Labels']['service_base']|escape:'html'}:<sup>*</sup></label>
		    &nbsp;&nbsp;
		    <select name="ServiceBaseStatus" id="ServiceBaseStatus">
			<option value="">Select Service Base Status</option>
			{foreach $sbStatuses as $status}
			    <option value="{$status.SBAuthorisationStatusID|escape:'html'}" {if $datarow.SBAuthorisationStatusID == $status.SBAuthorisationStatusID}selected="selected"{/if}>{$status.SBAuthorisationStatus|escape:'html'}</option>
			{/foreach}
		    </select>
		</p>
		    
		<p>
		    <label class="fieldLabel" for="RAStatusName" >{$page['Labels']['status_name']|escape:'html'}:<sup>*</sup></label>
		    &nbsp;&nbsp; 
		    <input type="text" class="text" name="RAStatusName" value="{$datarow.RAStatusName|escape:'html'}" id="RAStatusName" />
		</p>
                
		<p>
		    <label class="fieldLabel" for="ListOrder">{$page['Labels']['list_order']|escape:'html'}:<sup>*</sup></label>
		    &nbsp;&nbsp; 
		    <input  type="text" class="text"  name="ListOrder" value="{$datarow.ListOrder|escape:'html'}" id="ListOrder" />
		</p>
                
		<p>
		    <label class="fieldLabel" for="Colour">{$page['Labels']['colour']|escape:'html'}:<sup>*</sup></label>
		    &nbsp;&nbsp; 
		    <input  type="text" class="text"  name="Colour" value="{$datarow.Colour|escape:'html'}" id="Colour" style="width:270px;" />
		</p>
                
		<p>
		    <label class="fieldLabel" for="Subset">{$page['Labels']['specify_subset']|escape:'html'}:</label>
		    &nbsp;&nbsp;
		    <select name="Subset[]" id="Subset" multiple="multiple" style="width:305px;" class="text">
			{foreach from = $raStatuses item = rs}
			    <option value="{$rs.RAStatusID}" {if in_array($rs.RAStatusID, $datarow.Subset) || empty($datarow.Subset)}selected="selected"{/if}>
				{$rs.RAStatusName|escape:'html'}
			    </option>
			{/foreach}
		    </select>
		</p>

		<p>
		    <label class="fieldLabel" for="SubsetOnly">{$page['Labels']['subset_only']|escape:'html'}:<sup>*</sup></label>
		    &nbsp;&nbsp; 
		    <input type="radio" name="SubsetOnly" value="Yes" {if $datarow.SubsetOnly == "Yes"}checked="checked"{/if} />
		    <span class="text">Yes</span> 
		    <input type="radio" name="SubsetOnly" value="No" {if $datarow.SubsetOnly == "No"}checked="checked"{/if} />
		    <span class="text">No</span> 
		</p>

		<p>
		    <label class="fieldLabel" for="Final">Final Resolution Status:<sup>*</sup></label>
		    &nbsp;&nbsp; 
		    <input type="radio" name="Final" value="Yes" {if $datarow.Final == "Yes"}checked="checked"{/if} />
		    <span class="text">Yes</span> 
		    <input type="radio" name="Final" value="No" {if $datarow.Final == "No"}checked="checked"{/if} />
		    <span class="text">No</span> 
		</p>
		
		<p>
		    <label class="fieldLabel" for="Status">{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
		    &nbsp;&nbsp; 
		    {foreach $statuses as $status}
			<input type="radio" name="Status" value="{$status.Code}" {if $datarow.Status eq $status.Code}checked="checked"{/if} />
			<span class="text" >{$status.Name|escape:'html'}</span> 
		    {/foreach}    
		</p>

		<p>
		    <span class="bottomButtons">
			<input type="hidden" name="RAStatusID" value="{$datarow.RAStatusID|escape:'html'}" />
			<input type="hidden" name="RAStatusCode" value="{$datarow.RAStatusCode|escape:'html'}" />
			{if $datarow.RAStatusID != '' && $datarow.RAStatusID != '0'}
			     <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn" value="{$page['Buttons']['save']|escape:'html'}" />
			{else}
			    <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn" value="{$page['Buttons']['save']|escape:'html'}" />
			{/if}
			<br/>
			<input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;" value="{$page['Buttons']['cancel']|escape:'html'}" />
		    </span>
		</p>

	    </fieldset>    
                        
	</form>        
       
    </div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display:none;">   
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post" action="#" class="inline">
	<fieldset>
	    <legend title=""> {$form_legend|escape:'html'} </legend>
	    <p>
		<b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br/><br/>
	    </p>
	    <p>
		<span class= "bottomButtons">
		    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;" value="{$page['Buttons']['ok']|escape:'html'}" />
		</span>
	    </p>
	</fieldset>   
    </form>            
</div>    

    
 
{* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;">
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post" action="#" class="inline">
	<fieldset>
	    <legend title="">{$form_legend|escape:'html'}</legend>
	    <p>
		<b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br/><br/>
	    </p>
	    <p>
		<span class= "bottomButtons">
		    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;" value="{$page['Buttons']['ok']|escape:'html'}" />
		</span>
	    </p>
	</fieldset>   
    </form>            
</div>                            
                        
