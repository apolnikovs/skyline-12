import java.awt.Color;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.*;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.*;
import org.apache.commons.io.FileUtils;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;

import org.ini4j.Wini;

import com.fasterxml.jackson.databind.ObjectMapper;


public class ServiceActionTrackingReport {
	
	//Global vars
	public static BasicDataSource dataSource = new BasicDataSource();
	public static Map<String, Object> params = new HashMap<String, Object>();
	
	
	//Constructor
	public ServiceActionTrackingReport() throws Exception {

		//retrieving skyline's conf
		String curDir = System.getProperty("user.dir");
		String ini = curDir + "/application/config/application.ini";

		String dbName = null;
		String userName = null;
		String password = null;

		File f = new File(ini);
		if(f.exists()) {
			//config file exists
			Wini conf = new Wini(f);
			String[] tmp = conf.get("DataBase", "Conn").split("=");
			dbName = tmp[2].replace("\"", "");
			userName = conf.get("DataBase", "Username");
			password = conf.get("DataBase", "Password");
		} else {		
			//config file doesn't exist
			dbName = "skyline";
			userName = "root";
			password = "";
		}
		
		//initiating database
    	dataSource.setDriverClassName("com.mysql.jdbc.Driver");
    	dataSource.setUsername(userName);
    	if(password != null && !password.equals("") && !password.equals("\"\"")) {
    		dataSource.setPassword(password);
		}
    	
    	dataSource.addConnectionProperty("zeroDateTimeBehavior", "convertToNull");
    	dataSource.addConnectionProperty("allowMultiQueries", "true");
    	dataSource.setUrl("jdbc:mysql://localhost/" + dbName);
    	
    	//dataSource.setUrl("jdbc:mysql://localhost/" + dbName + "?zeroDateTimeBehavior=convertToNull&allowMultiQueries=true");
		
	}
	
	
	
	//Main function
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {

		@SuppressWarnings("unused")
		ServiceActionTrackingReport n = new ServiceActionTrackingReport();
		
    	ObjectMapper mapper = new ObjectMapper();
    	
    	String path = System.getProperty("user.dir");
    	
    	if(args.length != 0) {
    		String hash = args[0];
    		String pathTmp = path + "/temp/" + hash + ".json";
    		File input = new File(pathTmp);
    		params = mapper.readValue(input, Map.class);
    		input.delete();
    	} else {
    		//default params
    		params.put("clientID", "");
    		params.put("clientName", "Samsung");
    		//params.put("branchID", "");
    		//params.put("branchName", "Westfield Stratford");
    		params.put("dateFrom", "");
    		params.put("dateTo", "");
    		params.put("logo", "1000_logo.png");
    	}
		
		XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet sheet = wb.createSheet("Report");
        
        //log("PARAMS: " + params);
        
        //Logo
        String logoPath = "";
        if(params.get("logo") == null) {
        	logoPath = path + "/images/brandLogos/1000_logo.png";
        } else {
        	logoPath = path + "/images/brandLogos/" + params.get("logo");
        }
        InputStream is = new FileInputStream(logoPath);
        byte[] bytes = IOUtils.toByteArray(is);
        int pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
        is.close();

        CreationHelper helper = wb.getCreationHelper();
        Drawing drawing = sheet.createDrawingPatriarch();

        ClientAnchor anchor = helper.createClientAnchor();
        anchor.setCol1(6);
        anchor.setRow1(1);
        anchor.setDx1(1000);
        
        Picture pict = drawing.createPicture(anchor, pictureIdx);
        pict.resize();
        
        
        String pathSql = path + "/application/sql/service_action_tracking_report.sql";
        
        String q = FileUtils.readFileToString(new File(pathSql));
        
		String queryParams = "";
        if(params.containsKey("dateFrom") && params.get("dateFrom") != null && !params.get("dateFrom").equals("")) {
        	queryParams += " AND DateBooked >= '" + params.get("dateFrom") + "'";
        }
        if(params.containsKey("dateTo") && params.get("dateTo") != null && !params.get("dateTo").equals("")) {
        	queryParams += " AND DateBooked <= '" + params.get("dateTo") + "'";
        }
        if(params.containsKey("networkID") && params.get("networkID") != null && !params.get("networkID").equals("")) {
        	queryParams += " AND NetworkID = " + params.get("networkID");
        }
        if(params.containsKey("clientID") && params.get("clientID") != null && !params.get("clientID").equals("")) {
        	queryParams += " AND ClientID = " + params.get("clientID");
        }
        if(params.containsKey("branches") && params.get("branches") != null) {
        	ArrayList<String> branchList = (ArrayList<String>) params.get("branches");
        	if(!branchList.isEmpty()) {
        		queryParams += " AND BranchID IN(" + StringUtils.join(branchList, ",") + ")";
        	}
        }
        q = q.replace("#PARAMS#", queryParams);
        
        //dump the main query before running
        //log(q);
        
        ArrayList<String[]> data = query(q);
        
        //header
        String[] header = new String[11];
        header[0] = "Model No";
        header[1] = "Software Update";
        header[2] = "Education";
        header[3] = "Setup";
        header[4] = "Damaged ";
        header[5] = "Faulty Acc";
        header[6] = "Non-UK Model ";
        header[7] = "Unresolvable";
        header[8] = "Branch Repair";
        header[9] = "3rd Party Repair";
        header[10] = "Total";
        data.add(0, header);
        
        Map<String, XSSFCellStyle> styles = createStyles(wb);
        String sheetRef = sheet.getPackagePart().getPartName().getName();

        //save the template
        File template = new File("template.xlsx");
        FileOutputStream os = new FileOutputStream(template);
        wb.write(os);
        os.close();

        File tmp = File.createTempFile("sheet", ".xml");
        Writer fw = new FileWriter(tmp);
        generate(fw, styles, data);
        fw.close();
        
	    Calendar c = new GregorianCalendar();
	    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss.SS");
	    String now = format.format(c.getTime());
	    String fileName = "Service Action Tracking Report " + now;
        
        String pathFinal = path + "/reports/" + fileName + ".xlsx";
        
        FileOutputStream out = new FileOutputStream(new File(pathFinal));
        substitute(template, tmp, sheetRef.substring(1), out);
        out.close();

        //return generated file name to php
        System.out.println(fileName);
        
		System.gc();
        template.delete();
        
    }

    
	public static void log(String msg) {
		
		String curDir = System.getProperty("user.dir");
		String logPath = curDir + "/application/logs/java.log";
		
	    Calendar c = new GregorianCalendar();
	    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    String now = format.format(c.getTime());
		
    	try {
    	    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(logPath, true)));
    	    out.println(now + "  " + msg + "\n");
    	    out.close();
    	} catch (IOException e) {
    	    //error
    	}
    	
	}
	
	
    private static Map<String, XSSFCellStyle> createStyles(XSSFWorkbook wb) {
    	
        Map<String, XSSFCellStyle> styles = new HashMap<String, XSSFCellStyle>();
        XSSFDataFormat fmt = wb.createDataFormat();

        XSSFColor borderColor = new XSSFColor(Color.decode("0xA4A4A4"));
        
        XSSFCellStyle style1 = wb.createCellStyle();
        style1.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
        style1.setDataFormat(fmt.getFormat("0.0%"));
        styles.put("percent", style1);

        XSSFCellStyle style2 = wb.createCellStyle();
        style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
        style2.setDataFormat(fmt.getFormat("0.0X"));
        styles.put("coeff", style2);

        XSSFCellStyle style3 = wb.createCellStyle();
        style3.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
        style3.setDataFormat(fmt.getFormat("$#,##0.00"));
        styles.put("currency", style3);

        XSSFCellStyle style4 = wb.createCellStyle();
        style4.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
        style4.setDataFormat(fmt.getFormat("mmm dd"));
        styles.put("date", style4);

        XSSFCellStyle style5 = wb.createCellStyle();
        XSSFFont headerFont = wb.createFont();
        headerFont.setBold(true);
        style5.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style5.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        style5.setFont(headerFont);
        style5.setBorderBottom(CellStyle.BORDER_THIN);
        style5.setBottomBorderColor(borderColor);
        style5.setBorderTop(CellStyle.BORDER_THIN);
        style5.setTopBorderColor(borderColor);
        style5.setBorderRight(CellStyle.BORDER_THIN);
        style5.setRightBorderColor(borderColor);
        style5.setBorderLeft(CellStyle.BORDER_THIN);
        style5.setLeftBorderColor(borderColor);
        styles.put("header", style5);

        XSSFCellStyle darkGrayCenter = wb.createCellStyle();
        darkGrayCenter.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        darkGrayCenter.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        darkGrayCenter.setFont(headerFont);
        darkGrayCenter.setBorderBottom(CellStyle.BORDER_THIN);
        darkGrayCenter.setBottomBorderColor(borderColor);
        darkGrayCenter.setBorderTop(CellStyle.BORDER_THIN);
        darkGrayCenter.setTopBorderColor(borderColor);
        darkGrayCenter.setBorderRight(CellStyle.BORDER_THIN);
        darkGrayCenter.setRightBorderColor(borderColor);
        darkGrayCenter.setBorderLeft(CellStyle.BORDER_THIN);
        darkGrayCenter.setLeftBorderColor(borderColor);
        darkGrayCenter.setAlignment(CellStyle.ALIGN_CENTER);
        styles.put("darkGrayCenter", darkGrayCenter);

		XSSFCellStyle grayRowCenter = wb.createCellStyle();
		grayRowCenter.setFillForegroundColor(new XSSFColor(Color.decode("0xE8E8E8")));
		grayRowCenter.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		grayRowCenter.setBorderBottom(CellStyle.BORDER_THIN);
		grayRowCenter.setBottomBorderColor(borderColor);
		grayRowCenter.setBorderTop(CellStyle.BORDER_THIN);
		grayRowCenter.setTopBorderColor(borderColor);
		grayRowCenter.setBorderRight(CellStyle.BORDER_THIN);
		grayRowCenter.setRightBorderColor(borderColor);
		grayRowCenter.setBorderLeft(CellStyle.BORDER_THIN);
		grayRowCenter.setLeftBorderColor(borderColor);
		grayRowCenter.setAlignment(CellStyle.ALIGN_CENTER);
        styles.put("grayRowCenter", grayRowCenter);

		XSSFCellStyle grayRowLeft = wb.createCellStyle();
		grayRowLeft.setFillForegroundColor(new XSSFColor(Color.decode("0xE8E8E8")));
		grayRowLeft.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		grayRowLeft.setBorderBottom(CellStyle.BORDER_THIN);
		grayRowLeft.setBottomBorderColor(borderColor);
		grayRowLeft.setBorderTop(CellStyle.BORDER_THIN);
		grayRowLeft.setTopBorderColor(borderColor);
		grayRowLeft.setBorderRight(CellStyle.BORDER_THIN);
		grayRowLeft.setRightBorderColor(borderColor);
		grayRowLeft.setBorderLeft(CellStyle.BORDER_THIN);
		grayRowLeft.setLeftBorderColor(borderColor);
		grayRowLeft.setAlignment(CellStyle.ALIGN_LEFT);
        styles.put("grayRowLeft", grayRowLeft);
        
        
		XSSFCellStyle whiteRowCenter = wb.createCellStyle();
		whiteRowCenter.setFillForegroundColor(new XSSFColor(Color.decode("0xFFFFFF")));
		whiteRowCenter.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		whiteRowCenter.setBorderBottom(CellStyle.BORDER_THIN);
		whiteRowCenter.setBottomBorderColor(borderColor);
		whiteRowCenter.setBorderTop(CellStyle.BORDER_THIN);
		whiteRowCenter.setTopBorderColor(borderColor);
		whiteRowCenter.setBorderRight(CellStyle.BORDER_THIN);
		whiteRowCenter.setRightBorderColor(borderColor);
		whiteRowCenter.setBorderLeft(CellStyle.BORDER_THIN);
		whiteRowCenter.setLeftBorderColor(borderColor);
		whiteRowCenter.setAlignment(CellStyle.ALIGN_CENTER);
		styles.put("whiteRowCenter", whiteRowCenter);

		XSSFCellStyle whiteRowLeft = wb.createCellStyle();
		whiteRowLeft.setFillForegroundColor(new XSSFColor(Color.decode("0xFFFFFF")));
		whiteRowLeft.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		whiteRowLeft.setBorderBottom(CellStyle.BORDER_THIN);
		whiteRowLeft.setBottomBorderColor(borderColor);
		whiteRowLeft.setBorderTop(CellStyle.BORDER_THIN);
		whiteRowLeft.setTopBorderColor(borderColor);
		whiteRowLeft.setBorderRight(CellStyle.BORDER_THIN);
		whiteRowLeft.setRightBorderColor(borderColor);
		whiteRowLeft.setBorderLeft(CellStyle.BORDER_THIN);
		whiteRowLeft.setLeftBorderColor(borderColor);
		whiteRowLeft.setAlignment(CellStyle.ALIGN_LEFT);
		styles.put("whiteRowLeft", whiteRowLeft);

		XSSFCellStyle nameStyle = wb.createCellStyle();
        Font font = wb.createFont();
        font.setFontHeightInPoints((short)24);
        nameStyle.setFont(font);
        styles.put("name", nameStyle);
        
		XSSFCellStyle whiteBold = wb.createCellStyle();
        whiteBold.setFont(headerFont);
        styles.put("whiteBold", whiteBold);
        
        return styles;
    }

    
    
	private static void generate(Writer out, Map<String, XSSFCellStyle> styles, ArrayList<String[]> data) throws Exception {

        Calendar calendar = Calendar.getInstance();

        SpreadsheetWriter sw = new SpreadsheetWriter(out);
        sw.beginSheet(data);

        //Document header
        //sw.writeCustom("<row r=\"2\" ht=\"30\" customHeight=\"1\">");
        //sw.writeCustom("<row r=\"1\" spans=\"1:12\" s=\"1\">");
        sw.insertRow(0);
    	sw.createCell(0, "Service Action Report", styles.get("name").getIndex());
        sw.endRow();

        //Table header
        sw.insertRow(2);
        sw.createCell(0, "Client:", styles.get("whiteBold").getIndex());
        sw.createCell(1, (String)params.get("clientName"));
        sw.endRow();
        
        //Branch list
        sw.insertRow(3);
        sw.createCell(0, "Branches:", styles.get("whiteBold").getIndex());
        if(params.containsKey("branches") && params.get("branches") != null) {
        	@SuppressWarnings("unchecked")
			ArrayList<String> branchList = (ArrayList<String>) params.get("branches");
        	String branchIDs = "";
        	if(!branchList.isEmpty()) {
        		branchIDs += StringUtils.join(branchList, ",");
            	String branchQuery = "SELECT DISTINCT BranchName FROM branch WHERE BranchID IN(" + branchIDs + ")";
            	ArrayList<String[]> branchResult = query(branchQuery);
            	String[] branchTemp = new String[branchResult.size()];
            	for(int i = 0; i < branchResult.size(); i++) {
            		branchTemp[i] = branchResult.get(i)[0];
            	}
            	String branches = StringUtils.join(branchTemp, ", ");
                sw.createCell(1, StringEscapeUtils.escapeXml(branches));
        	}
        }
        sw.endRow();
        
	    Calendar c = new GregorianCalendar();
	    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	    SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd");
	    
        //Date from
        sw.insertRow(4);
        sw.createCell(0, "Date From:", styles.get("whiteBold").getIndex());
        //sw.createCell(1, (String)params.get("dateFrom"));
        if(!((String) params.get("dateFrom")).equals("")) {
        	sw.createCell(1, format.format(isoFormat.parse((String) params.get("dateFrom"))));
        }
        sw.endRow();
        
        //Date to
        sw.insertRow(5);
        sw.createCell(0, "Date To:", styles.get("whiteBold").getIndex());
        //sw.createCell(1, (String)params.get("dateTo"));
        if(!((String) params.get("dateTo")).equals("")) {
        	sw.createCell(1, format.format(isoFormat.parse((String) params.get("dateTo"))));
        }
        sw.endRow();
        
	    String now = format.format(c.getTime());
        sw.insertRow(6);
        sw.createCell(0, "Date Created:", styles.get("whiteBold").getIndex());
        sw.createCell(1, now);
        sw.endRow();
        
        sw.insertRow(8);
        for(int i = 0; i < data.get(0).length; i++) {
        	sw.createCell(i, data.get(0)[i], styles.get("header").getIndex());
        }
        sw.endRow();
        
		int lastRow = 0;
        
        //Data rows
        for(int rownum = 1; rownum < data.size(); rownum++) {

        	lastRow = rownum + 8;
        	
            sw.insertRow(rownum + 8);

            Double numVal;
            String stringVal;
            
            for(int i = 0; i < data.get(0).length; i++) {
            	
            	if(data.get(rownum)[i] == null) {
            		data.get(rownum)[i] = "";
            	}
            	
            	if(rownum % 2 == 0) {
            		
    				stringVal = stripControlChars(StringEscapeUtils.escapeXml(data.get(rownum)[i]));
    				if(i == 0) {
	        			try	{
	        				numVal = Double.parseDouble(stringVal);
	        				numVal = (double)(Math.round(numVal * 1000)) / 1000;
	            			sw.createCell(i, numVal, styles.get("whiteRowLeft").getIndex());
	        			} catch(NumberFormatException e) {
	        				//not a double
	            			sw.createCell(i, stringVal, styles.get("whiteRowLeft").getIndex());
	        			}
    				} else {
	        			try	{
	        				numVal = Double.parseDouble(stringVal);
	        				numVal = (double)(Math.round(numVal * 1000)) / 1000;
	            			sw.createCell(i, numVal, styles.get("whiteRowCenter").getIndex());
	        			} catch(NumberFormatException e) {
	        				//not a double
	            			sw.createCell(i, stringVal, styles.get("whiteRowCenter").getIndex());
	        			}
    				}
            		
            	} else {
            		
    				stringVal = stripControlChars(StringEscapeUtils.escapeXml(data.get(rownum)[i]));
    				if(i == 0) {
	        			try	{
	        				numVal = Double.parseDouble(stringVal);
	        				numVal = (double)(Math.round(numVal * 1000)) / 1000;
	            			sw.createCell(i, numVal, styles.get("grayRowLeft").getIndex());
	        			} catch(NumberFormatException e) {
	        				//not a double
	            			sw.createCell(i, stringVal, styles.get("grayRowLeft").getIndex());
	        			}
    				} else {
	        			try	{
	        				numVal = Double.parseDouble(stringVal);
	        				numVal = (double)(Math.round(numVal * 1000)) / 1000;
	            			sw.createCell(i, numVal, styles.get("grayRowCenter").getIndex());
	        			} catch(NumberFormatException e) {
	        				//not a double
	            			sw.createCell(i, stringVal, styles.get("grayRowCenter").getIndex());
	        			}
    				}
            		
            	}
            	
            }
            
            sw.endRow();

            calendar.roll(Calendar.DAY_OF_YEAR, 1);
        }
        
        //Totals
        sw.insertRow(lastRow + 2);
        sw.createCell(0, "Record Count:", styles.get("header").getIndex());
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 1).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(10, 1).formatAsString() + ":" + new CellReference(lastRow, 1).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 2).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(10, 2).formatAsString() + ":" + new CellReference(lastRow, 2).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 3).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(10, 3).formatAsString() + ":" + new CellReference(lastRow, 3).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 4).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(10, 4).formatAsString() + ":" + new CellReference(lastRow, 4).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 5).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(10, 5).formatAsString() + ":" + new CellReference(lastRow, 5).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 6).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(10, 6).formatAsString() + ":" + new CellReference(lastRow, 6).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 7).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(10, 7).formatAsString() + ":" + new CellReference(lastRow, 7).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 8).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(10, 8).formatAsString() + ":" + new CellReference(lastRow, 8).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 9).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(10, 9).formatAsString() + ":" + new CellReference(lastRow, 9).formatAsString() + ")</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 2, 10).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>" + "SUBTOTAL(9," + new CellReference(10, 10).formatAsString() + ":" + new CellReference(lastRow, 10).formatAsString() + ")</f></c>");
        sw.endRow();

        //Percentages
        sw.insertRow(lastRow + 3);
        sw.createCell(0, "Percentage:", styles.get("header").getIndex());
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 1).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 1).formatAsString() + "/" + new CellReference(lastRow + 2, 10).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 2).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 2).formatAsString() + "/" + new CellReference(lastRow + 2, 10).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 3).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 3).formatAsString() + "/" + new CellReference(lastRow + 2, 10).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 4).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 4).formatAsString() + "/" + new CellReference(lastRow + 2, 10).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 5).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 5).formatAsString() + "/" + new CellReference(lastRow + 2, 10).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 6).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 6).formatAsString() + "/" + new CellReference(lastRow + 2, 10).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 7).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 7).formatAsString() + "/" + new CellReference(lastRow + 2, 10).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 8).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 8).formatAsString() + "/" + new CellReference(lastRow + 2, 10).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 9).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 9).formatAsString() + "/" + new CellReference(lastRow + 2, 10).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.writeCustom("<c r=\"" + new CellReference(lastRow + 3, 10).formatAsString() + "\" s=\"" + styles.get("darkGrayCenter").getIndex() + "\"><f>(ROUND(((" + new CellReference(lastRow + 2, 10).formatAsString() + "/" + new CellReference(lastRow + 2, 10).formatAsString() + ") * 100),2))&amp;\"%\"</f></c>");
        sw.endRow();

        
        sw.endSheet(data);
        
    }

    
    
    private static void substitute(File zipfile, File tmpfile, String entry, OutputStream out) throws IOException {
    	
        ZipFile zip = new ZipFile(zipfile);

        ZipOutputStream zos = new ZipOutputStream(out);

        @SuppressWarnings("unchecked")
        Enumeration<ZipEntry> en = (Enumeration<ZipEntry>) zip.entries();
        while (en.hasMoreElements()) {
            ZipEntry ze = en.nextElement();
            if(!ze.getName().equals(entry)){
                zos.putNextEntry(new ZipEntry(ze.getName()));
                InputStream is = zip.getInputStream(ze);
                copyStream(is, zos);
                is.close();
            }
        }
        zos.putNextEntry(new ZipEntry(entry));
        InputStream is = new FileInputStream(tmpfile);
        copyStream(is, zos);
        is.close();

        zos.close();
        zip.close();
    }

    
    
    private static void copyStream(InputStream in, OutputStream out) throws IOException {
        byte[] chunk = new byte[1024];
        int count;
        while ((count = in.read(chunk)) >=0 ) {
          out.write(chunk,0,count);
        }
    }

    
    
    public static class SpreadsheetWriter {
    	
        private final Writer _out;
        private int _rownum;

        
        public SpreadsheetWriter(Writer out){
            _out = out;
        }
        
        
        public void beginSheet(ArrayList<String[]> data) throws IOException {
            _out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        	_out.write("<worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\">");
        	_out.write("<dimension ref=\"A1\"/>");
        	_out.write("<sheetViews><sheetView workbookViewId=\"0\" showGridLines=\"false\" tabSelected=\"true\"><pane ySplit=\"9\" topLeftCell=\"A10\" activePane=\"bottomLeft\" state=\"frozen\"/></sheetView></sheetViews>");
        	_out.write("<sheetFormatPr defaultRowHeight=\"15.0\"/>");
        	
        	_out.write("<cols>");
        	float[] colWidths = ExcelHelper.calculateColumnWidths(data);	
        	for(int i = 0; i < colWidths.length; i++) {
        		_out.write("<col min=\"" + (i + 1) + "\" max=\"" + (i +1) + "\" width=\"" + colWidths[i] + "\" customWidth=\"true\" bestFit=\"true\"/>");
        	}
        	_out.write("</cols>");
            
        	_out.write("<sheetData>");
        	
        }

        
        public void endSheet(ArrayList<String[]> data) throws IOException {
            _out.write("</sheetData>");
            
            /*
        	_out.write("<mergeCells count=\"1\">");
        	_out.write("<mergeCell ref=\"A1:C2\"/>");
        	_out.write("</mergeCells>");
            */
            
            String end = new CellReference((data.size() - 1), (data.get(1).length - 1)).formatAsString();
            _out.write("<autoFilter ref=\"A9:" + end + "\" />");
            
            _out.write("<drawing r:id=\"rId1\"/>");
            
            _out.write("</worksheet>");
        }

        
        public void insertRow(int rownum) throws IOException {
            _out.write("<row r=\"" + (rownum + 1) + "\">");
            this._rownum = rownum;
        }


        public void endRow() throws IOException {
            _out.write("</row>");
        }


        public void createCell(int columnIndex, String value, int styleIndex) throws IOException {
            String ref = new CellReference(_rownum, columnIndex).formatAsString();
            _out.write("<c r=\"" + ref + "\" t=\"inlineStr\"");
            if(styleIndex != -1) {
            	_out.write(" s=\"" + styleIndex + "\"");
            }
            _out.write(">");
            _out.write("<is><t>" + value + "</t></is>");
            _out.write("</c>");
        }

        
        public void createCell(int columnIndex, String value) throws IOException {
            createCell(columnIndex, value, -1);
        }

        
        public void createCell(int columnIndex, double value, int styleIndex) throws IOException {
            String ref = new CellReference(_rownum, columnIndex).formatAsString();
            _out.write("<c r=\"" + ref + "\" t=\"n\"");
            if(styleIndex != -1) { 
            	_out.write(" s=\"" + styleIndex + "\"");
            }
            _out.write(">");
            _out.write("<v>" + value + "</v>");
            _out.write("</c>");
        }

        
        public void createCell(int columnIndex, double value) throws IOException {
            createCell(columnIndex, value, -1);
        }

        
        public void createCell(int columnIndex, Calendar value, int styleIndex) throws IOException {
            createCell(columnIndex, DateUtil.getExcelDate(value, false), styleIndex);
        }
        
        
        public void writeCustom(String custom) throws IOException {
        	_out.write(custom);
        }
        
        
    }
    
    
    
//==========================================================================================================
    
   
    //Query with params function
	public static ArrayList<String[]> query(Map<String, Object> params, String q) throws Exception {
    
		ArrayList<String[]> result = new ArrayList<String[]>();
		
    	NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

    	MapSqlParameterSource args = new MapSqlParameterSource();
    	
    	for(Map.Entry<String, Object> entry : params.entrySet()) {
    	    String key = entry.getKey();
    	    Object value = entry.getValue();
    		args.addValue(key, value);
    	}
    	
    	SqlRowSet rs = jdbcTemplate.queryForRowSet(q, args);
    	
    	SqlRowSetMetaData meta = rs.getMetaData();
    	int colCount = meta.getColumnCount();
    	
		while(rs.next()) {
			String[] row = new String[colCount];
			for(int i = 0; i < colCount; i++) {
				row[i] = rs.getString(i + 1);
			}
			result.add(row);
		}
		
    	return result;
    	
    }
    
	
	
	//Query without params function
	public static ArrayList<String[]> query(String q) throws Exception {
	    
		ArrayList<String[]> result = new ArrayList<String[]>();
		
    	JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

    	SqlRowSet rs = jdbcTemplate.queryForRowSet(q);
    	
    	SqlRowSetMetaData meta = rs.getMetaData();
    	int colCount = meta.getColumnCount();
    	
		while(rs.next()) {
			String[] row = new String[colCount];
			for(int i = 0; i < colCount; i++) {
				row[i] = rs.getString(i + 1);
			}
			result.add(row);
		}
		
    	return result;
    	
    }

	
	
	public static String stripControlChars(String iString) {
	    StringBuffer result = new StringBuffer(iString);
	    int idx = result.length();
	    while (idx-- > 0) {
	        if(result.charAt(idx) < 0x20 && result.charAt(idx) != 0x9 && result.charAt(idx) != 0xA && result.charAt(idx) != 0xD) {
	            result.deleteCharAt(idx);
	        }
	    }
	    return result.toString();
	}	
	
    
}
